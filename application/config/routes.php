<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['book/delete/(:num)'] = 'dashboard/BookController/delete/$1';
$route['book/update/(:num)'] = 'dashboard/BookController/update/$1';
$route['book/edit/(:num)'] = 'dashboard/BookController/edit/$1';
$route['book/create'] = 'dashboard/BookController/Create';


$route['category/create']='dashboard/CategoryController/create';
$route['category/edit/(:num)']='dashboard/CategoryController/edit/$1';
$route['category/delete/(:num)']='dashboard/CategoryController/delete/$1';
$route['category/update'] = 'dashboard/CategoryController/update';

$route['admin/order/confirm/(:num)'] = "dashboard/OrdersController/confirm/$1";
$route['admin/order/complete/(:num)'] = "dashboard/OrdersController/complete/$1";
$route['admin/customer/order_history/(:num)'] = "Admin/order_history/$1";

$route['admin/sign_in']  = "AdminAccountController/sign_in";
$route['admin/login']  = "AdminAccountController/login";
$route['admin/account_update']  = "AdminAccountController/update";

$route['account/create']='AccountController/register';
$route['account/update']='AccountController/update';
$route['account/login'] = 'AccountController/login';
$route['account/logout'] = 'AccountController/logout';
$route['account/resend']='AccountController/resend';
$route['account/activate'] = 'AccountController/activate';
$route['account/forget_password'] = 'AccountController/forget_password';
$route['account/password_reset'] = 'AccountController/password_reset';


$route['account/reset_password/(:any)'] = 'Users/reset_password/$1';
$route['book/details/(:num)'] = "Users/book/$1";

$route['sign-up'] = "Users/sign_up";
$route['forget-password'] = "Users/forget_password";
$route['unactivated'] = "Users/unactivated";
$route['sign-in'] = "Users/sign_in";
$route['search'] = "Users/search";
$route['books']="Users/books";
$route['category/(:num)/(:any)']="Users/category_book/$1";


$route['reader/read/(.*).pdf'] = "Reader/read/$1";
$route['reader/getfile/(.*).pdf'] = "Reader/getfile/$1";

$route['seeder']="Users/seeder";
$route['account/activated'] ='Users/activated';

#sales.ng

$route['book/(:num)'] = 'User/book/$1';
$route['default_controller'] = 'Users';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
