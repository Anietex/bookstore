<?php


class OrdersModel extends CI_Model
{
    function __construct()
    {
        $this->load->database();
    }

    public function create($trans_ref,$customer_id,$phone_no,$email,$address,$amount){
        $data = ["transaction_ref" =>$trans_ref,
            'customer_id'=>$customer_id,
            'phone_no'=>$phone_no,
            'email'=>$email,
            'amount_payed'=>$amount,
            'shipping_address'=>$address,
            'order_date'=>date('Y-m-d')];
        if($this->db->insert('orders',$data,true))
            return $this->db->insert_id();
        return false;
    }

    public function delete(){

    }

    public function getOrder($order_id){
        $sql = "Select orders.*, customers.first_name, customers.last_name from orders INNER JOIN  customers on orders.customer_id = customers.customer_id WHERE order_id= ?";
        return $this->db->query($sql,[$order_id])->row();
    }

    public function getOrders($start=null,$limit=null){
        $sql = "Select orders.*, customers.first_name, customers.last_name from orders INNER JOIN  customers on orders.customer_id = customers.customer_id order by order_date desc ";
        return $this->db->query($sql)->result();
    }


    public function getCustomerOrders($customer_id){
        $sql = "Select orders.*, customers.first_name, customers.last_name from orders INNER JOIN  customers on orders.customer_id = customers.customer_id WHERE orders.customer_id = ?";
        return $this->db->query($sql,[$customer_id])->result();

    }

    public function getCustomerOrder($customer_id,$order_id){
        return $this->db->get_where('orders',['customer_id'=>$customer_id,'order_id'=>$order_id])->row();
    }

    public function updateConfirm($order_id){
        return $this->db->query('Update orders set confirmed = TRUE WHERE order_id = ?',[$order_id]);
    }

    public function updateComplete($order_id){
        return $this->db->query('Update orders set completed = TRUE WHERE order_id = ?',[$order_id]);
    }

    public function getTotalDailySales($date,$book_id=null){
        $sql = "SELECT  sum(books.hardcopy_price*ordered_items.quantity) as total  FROM orders INNER JOIN ordered_items ON orders.order_id = ordered_items.order_id INNER JOIN books ON ordered_items.book_id = books.book_id where order_date = ?";
       if(empty($book_id))
            return $this->db->query($sql,[$date])->row()->total;
       $sql.=" and  ordered_items.book_id = ?";
        return $this->db->query($sql,[$date,$book_id])->row()->total;
    }

    public function getTotalSales(){
        $sql = "SELECT SUM(amount_payed) as total from orders";
        return $this->db->query($sql)->row()->total;
    }

    public function getUnconfirmedOrders(){
        $sql = "Select orders.*, customers.first_name, customers.last_name from orders INNER JOIN  customers on orders.customer_id = customers.customer_id WHERE confirmed = 0 and completed = 0";
        return $this->db->query($sql)->result();
    }

    public function getConfirmedOrders(){
        $sql = "Select orders.*, customers.first_name, customers.last_name from orders INNER JOIN  customers on orders.customer_id = customers.customer_id  WHERE confirmed =1 and completed = 0";
        return $this->db->query($sql)->result();
    }

    public function getIncompleteOrders(){
        $sql = "Select orders.*, customers.first_name, customers.last_name from orders INNER JOIN  customers on orders.customer_id = customers.customer_id WHERE completed = 0";
        return $this->db->query($sql)->result();
    }

    public function getCompletedOrders(){
        $sql = "Select orders.*, customers.first_name, customers.last_name from orders INNER JOIN  customers on orders.customer_id = customers.customer_id WHERE completed =1";
        return $this->db->query($sql)->result();
    }
}