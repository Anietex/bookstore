<?php


class EbookModel extends CI_Model
{
    function __construct()
    {
        $this->load->database();
    }

    public function create($customer_id,$book_id,$trans_id,$amount,$udid){
        $data = ['book_id' =>$book_id,
            'customer_id' =>$customer_id,
            'trans_id' => $trans_id,
            'amount_payed' =>$amount,
            'download_id' =>$udid,
            'date_sold'=>date('Y-m-d '),
            'expires_at' =>time()+60*60*24];
       return $this->db->insert('ebook_download',$data);
    }

    public function getEbook($criteria){
        return $this->db->get_where('ebook_download',$criteria)->row();
    }

    public function getCustomerEbooks($customer_id){
        $sql = "Select ebook_download.*, books.title, books.cover_photo_file from ebook_download INNER JOIN books ON ebook_download.book_id = books.book_id  WHERE ebook_download.customer_id = ?";
        return $this->db->query($sql,[$customer_id])->result();
    }

    public function increaseDownload($download_id){
        $sql = "UPDATE ebook_download set download_count = download_count+1 WHERE download_id = ?";
        return  $this->db->query($sql,[$download_id]);
    }

    public function getSales(){
        $sql = "Select ebook_download.*, books.title, customers.first_name, customers.last_name from ebook_download INNER JOIN books ON ebook_download.book_id = books.book_id INNER JOIN customers on ebook_download.customer_id = customers.customer_id";
        return $this->db->query($sql)->result();
    }

    public function getTotalDailySales($date,$book_id=null){
        $sql = "SELECT SUM(amount_payed) as total from ebook_download WHERE date_sold =?";
        if(empty($book_id))
            return $this->db->query($sql,[$date])->row()->total;

        $sql .=" and book_id = ?";
        return $this->db->query($sql,[$date,$book_id])->row()->total;
    }

    public function getTotalSales(){
        $sql = "SELECT SUM(amount_payed) as total from ebook_download";
        return $this->db->query($sql)->row()->total;
    }

    public function getBookSalesCount($book_id){
        return $this->db->query("Select count(*) as total from ebook_download where book_id = ?",[$book_id])->row()->total;
    }
}
