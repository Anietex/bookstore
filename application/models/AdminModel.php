<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 5/22/2018
 * Time: 3:00 PM
 */

class AdminModel extends  CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function getAdmin($username){
        return $this->db->get_where('admin',['username'=>$username])->row();
    }


    public function update($pass){
        $data=['username'=>$_POST['username'],
            'password'=>empty($_POST['password'])?$pass:password_hash($_POST['password'],1)];
        return $this->db->update('admin',$data,['username'=>$_SESSION['username']]);
    }
}