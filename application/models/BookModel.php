<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BookModel extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }


    public function getRandomBooks($start = 0, $limit =10){
        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id  where copies >0 or (ebook_file is not null) order by rand() Limit ? , ?',[$start,$limit])->result();

    }


    public function getSearchedBook($term,$start = 0, $limit =10){

        if($start ===null || $limit===null ){
            return $this->db->
            query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id where(MATCH(title,author) AGAINST(?))  and (copies >0 and (ebook_file is not null or ebook_file <> "")) order by date_added desc',[$term])->result();
        }
        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id  where(MATCH(title,author) AGAINST(?))  and (copies >0 or (ebook_file is not null)) order by date_added desc Limit ? , ?',[$term,$start,$limit])->result();

    }




    public function getBooks($start = null, $limit =null){

       if($start ===null || $limit===null ){
           return $this->db->
           query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id where copies >0 and (ebook_file is not null or ebook_file <> "") order by date_added desc')->result();
       }
        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id  where copies >0 or (ebook_file is not null) order by date_added desc Limit ? , ?',[$start,$limit])->result();

    }





    public function getBooksAdmin($start = null, $limit =null){
        if($start ===null || $limit===null ){
            return $this->db->
            query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id where deleted = false order by date_added desc')->result();
        }
        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id where deleted = false order by date_added desc Limit ? , ?',[$start,$limit])->result();

    }











    public function  getBooksInCategory($cat_id,$start=null,$limit=null){
        if($start ===null || $limit===null ){
            return $this->db->
            query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id WHERE books.cat_id = ?',[$cat_id])->result();
        }

        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id WHERE books.cat_id = ? Limit ? , ?',[$cat_id,$start,$limit])->result();

    }


    public function getPopularBooks($start = 0, $limit =6){
        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id where copies >0 and (ebook_file is not null or ebook_file <> "") order by sales DESC  Limit ? , ?',[$start,$limit])->result();

    }

    public function getNewBooks($start = 0, $limit =6){
        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id where copies >0 and (ebook_file is not null or ebook_file <> "") ORDER BY date_added DESC  Limit ? , ?',[$start,$limit])->result();

    }

    public function getBook($book_id){
        return $this->db->
        query('SELECT books.*, categories.cat_name from books INNER JOIN categories on books.cat_id = categories.cat_id WHERE book_id = ?',[$book_id])->row();

       // return $this->db->get_where('books',['book_id'=>$book_id])->row();
    }

    public function create($ebook,$cover){
        $data=['title'=>$_POST['book_title'],
            'cat_id'=>$_POST['category'],
            'author'=>$_POST['author'],
            'year_published'=>$_POST['year'],
            'pages'=>$_POST['pages'],
            'copies'=>$_POST['copies'],
            'hardcopy_price'=>$_POST['hardcopy_price'],
            'ebook_price'=>$_POST['ebook_price'],
            'read_price'=>$_POST['read_price'],
            'description'=>$_POST['description'],
            'ebook_file'=>$ebook,
            'edition'=>$_POST['edition'],
            'isbn' => $_POST['isbn'],
            'date_added'=>date('Y-m-d'),
            'cover_photo_file'=>$cover];
        return $this->db->insert('books',$data,true);
    }

    public function delete($book_id){
       return $this->db->where('book_id',$book_id)->delete('books');
    }

    public function softDelete($book_id){
         $this->db->set('deleted',true);
       return  $this->db->where('book_id',$book_id)->update('books');
    }

    public function update($book_id,$ebook,$cover){
        $data=['title'=>$_POST['book_title'],
            'cat_id'=>$_POST['category'],
            'author'=>$_POST['author'],
            'year_published'=>$_POST['year'],
            'pages'=>$_POST['pages'],
            'copies'=>$_POST['copies'],
            'hardcopy_price'=>$_POST['hardcopy_price'],
            'ebook_price'=>$_POST['ebook_price'],
            'read_price'=>$_POST['read_price'],
            'description'=>$_POST['description'],
            'ebook_file'=>$ebook,
            'edition'=>$_POST['edition'],
            'isbn' => $_POST['isbn'],
            'cover_photo_file'=>$cover];
        $this->db->set($data);
        $this->db->where('book_id',$book_id);
        return $this->db->update('books');
    }

    public function increaseSales($book_id,$qty){
       return $this->db->query("UPDATE books SET sales = sales+ ? WHERE book_id = ?",[$qty,$book_id]);
    }

    public function decreaseStock($book_id,$qty){
       return $this->db->query("UPDATE books SET copies = copies-? WHERE book_id = ?",[$qty,$book_id]);
    }

    public function seed($title,$cat,$author,$year,$pages,$copies,$hard,$ebook,$read,$des,$sales,$cover,$date){
        $data=['title'=>$title,
            'cat_id'=>$cat,
            'author'=>$author,
            'year_published'=>$year,
            'pages'=>$pages,
            'copies'=>$copies,
            'hardcopy_price'=>$hard,
            'ebook_price'=>$ebook,
            'read_price'=>$read,
            'description'=>$des,
            'sales'=>$sales,
            'ebook_file'=>$ebook,
            'cover_photo_file'=>$cover,
            'date_added' =>$date];
        return $this->db->insert('books',$data,true);
    }


}