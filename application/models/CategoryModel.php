<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CategoryModel extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function create(){
       return $this->db->insert('categories',['cat_name' =>$_POST['cat_name']]);
    }

    public function getCategories(){
        return $this->db->get("categories")->result();
    }

    public function getCategory($cat_id){
        return $this->db->get_where("categories",['cat_id'=>$cat_id])->row();
    }

    public function update(){
       $this->db->set('cat_name',$_POST['cat_name']);
       $this->db->where('cat_id',$_POST['cat_id']);
       return $this->db->update('categories');
    }


    public function delete($cat_id){
        $this->db->where('cat_id',$cat_id);
        return $this->db->delete('categories');
    }

    public function categoryExist($cat_name){
         return $this->db->get_where("categories",['cat_name'=>$cat_name])->row();
    }
}