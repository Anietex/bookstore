<?php

class ActivationModel extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }


    public function getCustomer($token){
       return $this->db->query("SELECT activations.*, customers.activated from activations INNER JOIN customers on activations.customer_id  = customers.customer_id where activation_token = ?",[$token])->row();
    }


    public function setToken($customer_id,$token){
        $data=['customer_id' =>$customer_id,
            'activation_token' =>$token,
            'expire_at' =>time()+86400];
        return $this->db->insert('activations',$data);

    }




}