<?php


class OrderedItemModel extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function create($order_id,$book_id,$quantity){
        $data = ['order_id'=>$order_id,
            'book_id'=>$book_id,
            'quantity'=>$quantity];
        return $this->db->insert('ordered_items',$data,true);
    }

    public function getOrderedItems($order_id){
        $sql = "SELECT ordered_items.*,books.title,books.hardcopy_price as cost, hardcopy_price*ordered_items.quantity as total_cost  from ordered_items INNER JOIN books ON books.book_id = ordered_items.book_id WHERE order_id = ?";
        return $this->db->query($sql,[$order_id])->result();
    }


    public function getBookSalesCount($book_id){
        return $this->db->query("Select sum(quantity) as total from ordered_items where book_id = ?",[$book_id])->row()->total;
    }




}