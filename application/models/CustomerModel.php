<?php

class CustomerModel extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function create(){
        $data = ['first_name' => $_POST['first_name'],
            'last_name' =>$_POST['last_name'],
            'gender' =>$_POST['gender'],
            'email' =>$_POST['email'],
            'phone_no'=>$_POST['phone_no'],
            'address'=>$_POST['address'],
            'password'=>password_hash($_POST['password'],1)];
       return $this->db->insert('customers',$data,true);
    }




    public function updateDetails($old_pass){
        $data = ['first_name' => $_POST['first_name'],
            'last_name' =>$_POST['last_name'],
            'gender' =>$_POST['gender'],
            'email' =>$_POST['email'],
            'phone_no'=>$_POST['phone_no'],
            'address'=>$_POST['address'],
            'password'=>empty($_POST['password'])?$old_pass:password_hash($_POST['password'],1)];

        return $this->db->update('customers',$data,['customer_id'=>$_SESSION['customer_id']]);
      //  return $this->db->insert('customers',$data,true);
    }

    public function getCustomers(){
        return $this->db->get('customers')->result();
    }

    public function update($customer_id,$data){
        $this->db->set($data);
        $this->db->where('customer_id',$customer_id);
        return $this->db->update('customers');
    }



    public function getTotalCustomers(){
        return count($this->getCustomers());
    }

    public function activate($id){
        $data=['activated'=>true];
        $this->db->set($data);
        $this->db->where('customer_id',$id);
        return $this->db->update('customers');
    }

    public function emailExist($email){
        return $this->db->get_where('customers',['email'=>$email])->row() != null;
    }

    public function phoneExist($phone){
        return $this->db->get_where('customers',['phone_no'=>$phone])->row() !=null;
    }

    public function getCustomer($criteria){
        return $this->db->get_where('customers',$criteria)->row();
    }


    public function getLogin($field){
        return $this->db->query('SELECT * FROM customers where email = ? or phone_no = ?',[$field,$field])->row();
    }


}