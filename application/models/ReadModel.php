<?php

class ReadModel extends CI_Model
{
    function __construct()
    {
        $this->load->database();
    }


    public function create($customer_id,$book_id,$trans_ref,$amount,$urid){
        $data = ['book_id'=>$book_id,
            'customer_id'=>$customer_id,
            'identifier'=>$urid,
            'amount_payed'=>$amount,
            'date_sold'=>date('Y-m-d'),
            'trans_ref'=>$trans_ref];

        return $this->db->insert('read_online',$data);
    }


    public function getBook($urid,$customer_id){
        return $this->db->get_where('read_online',['identifier'=>$urid,'customer_id'=>$customer_id])->row();
    }

    public  function getCustomerBooks($customer_id){
        $sql = "SELECT read_online.*, books.title, books.cover_photo_file from read_online INNER JOIN books ON read_online.book_id = books.book_id  where read_online.customer_id = ?";
        return $this->db->query($sql,[$customer_id])->result();
    }

    public function getSales(){
        $sql = "Select read_online.*, books.title, customers.first_name, customers.last_name from read_online INNER JOIN books ON read_online.book_id = books.book_id INNER JOIN customers on read_online.customer_id = customers.customer_id";
        return $this->db->query($sql)->result();
    }

    public function getTotalDailySales($date,$book_id=null){
        $sql = "SELECT SUM(amount_payed) as total from read_online WHERE date_sold =?";
        if(empty($book_id))
             return $this->db->query($sql,[$date])->row()->total;

        $sql.=" and book_id =?";
        return $this->db->query($sql,[$date,$book_id])->row()->total;
    }

    public function getTotalSales(){
        $sql = "SELECT SUM(amount_payed) as total from read_online";
        return $this->db->query($sql)->row()->total;
    }


    public function getBookSalesCount($book_id){
        return $this->db->query("Select count(*) as total from read_online where book_id = ?",[$book_id])->row()->total;
    }


}