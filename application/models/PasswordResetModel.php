<?php

class PasswordResetModel extends CI_Model
{
    function __construct()
    {
        $this->load->database();
    }


    public function create($customer_id,$hash){
        $data = ['customer_id'=>$customer_id,
            'hash'=>$hash];
        return $this->db->insert('password_resets',$data);
    }

    public function delete($customer_id){
        return $this->db->where('customer_id',$customer_id,true)->delete('password_resets');
    }

    public function getResetData($hash){
        return $this->db->get_where('password_resets',['hash'=>$hash])->row();
    }


}