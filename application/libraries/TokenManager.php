<?php

class TokenManager
{
    private  $CI ;

   public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('CustomerModel');
    }

    public function setToken($customer_id){

        $ip = $_SERVER['REMOTE_ADDR'];
        $ua =  $_SERVER['HTTP_USER_AGENT'];
        $token = bin2hex(openssl_random_pseudo_bytes(16));
        $data = ['ip_address'=>$ip,'last_browser'=>$ua,'remember_token'=>$token];
        setcookie('rmt',$token,time()+60*60*24*30,'/','',false,true);
        return $this->CI->CustomerModel->update($customer_id,$data);
    }

    public function tokenExist(){
        return isset($_COOKIE['rmt']);
    }

    public function clearToken(){
        setcookie('rmt','',time()-(60*60*24*30+3),'/','',false,true);
    }

    public  function AuthToken()
    {
        $customer = $this->CI->CustomerModel->getCustomer(['remember_token' => $_COOKIE['rmt']]);
        if ($customer) {
            $browser = $_SERVER['HTTP_USER_AGENT'];
            $ip = $_SERVER['REMOTE_ADDR'];

            if ($customer->ip_address == $ip && $browser == $customer->last_browser) {
                $_SESSION['customer_id'] = $customer->customer_id;
                $_SESSION['first_name'] = $customer->first_name;
                $this->setToken($customer->customer_id);
                return true;
            }
        }
        return false;
    }
}