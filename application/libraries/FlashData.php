<?php

class FlashData
{
    public static function set($key,$value){
        $_SESSION[$key] = $value;
    }

    public static function get($key,$default=''){
        $value = $default;
        if(isset($_SESSION[$key])){
            $value = $_SESSION[$key];
            unset($_SESSION[$key]);
        }
        return $value;
    }
}