<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardHelper
{

    public static function generateCategory($selected=''){
        $CI =& get_instance();
        $CI->load->model('CategoryModel');
        $cm = new CategoryModel();
        $cats = $cm->getCategories();


        foreach ($cats as $cat){
            if($cat->cat_name == $selected)
                print '<option value="'.$cat->cat_id.'" selected>'.$cat->cat_name.'</option>';
            else
                echo '<option value="'.$cat->cat_id.'">'.$cat->cat_name.'</option>';
        }
    }

}