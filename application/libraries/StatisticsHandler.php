<?php

class StatisticsHandler
{

    private $CI;
  function __construct()
  {
      $this->CI=&get_instance();
      $this->CI->load->model(['OrdersModel','EbookModel','ReadModel','OrderedItemModel']);
  }


  public function genSalesJSON(){

      $dates = $this->genDates(30);
      $hardCopy = [];
      $ebook = [];
      $reading = [];

      foreach ($dates as $date){
          array_push($hardCopy,$this->getValue($this->CI->OrdersModel->getTotalDailySales($date)));
          array_push($ebook,$this->getValue($this->CI->EbookModel->getTotalDailySales($date)));
          array_push($reading,$this->getValue($this->CI->ReadModel->getTotalDailySales($date)));
      }
     return ['days' =>$dates,'orders'=>$hardCopy,'ebook'=>$ebook,'readings'=>$reading];
  }


  public function  getBookSalesJSON($book_id){
      $dates = $this->genDates(10);
      $hardCopy = [];
      $ebook = [];
      $reading = [];

      foreach ($dates as $date){
          array_push($hardCopy,$this->getValue($this->CI->OrdersModel->getTotalDailySales($date,$book_id)));
          array_push($ebook,$this->getValue($this->CI->EbookModel->getTotalDailySales($date,$book_id)));
          array_push($reading,$this->getValue($this->CI->ReadModel->getTotalDailySales($date,$book_id)));
      }
      return ['sales_json'=>['days' =>$dates,
          'orders'=>$hardCopy,
          'ebook'=>$ebook,
          'readings'=>$reading,
          ],'buying_stat'=>$this->getBuyingStatistics($book_id)];
   }

  public function getOrdersStatistics(){
      $total = count($this->CI->OrdersModel->getOrders());
      $completed = count($this->CI->OrdersModel->getCompletedOrders());
      $confirmed = count($this->CI->OrdersModel->getConfirmedOrders());
      $unconfirmed = count($this->CI->OrdersModel->getUnconfirmedOrders());

     $pCompleted = $completed/$total*100;
     $pConfirm = $confirmed/$total*100;
     $pUnconfirmed = $unconfirmed/$total*100;

     return ['completed'=>$pCompleted,
         'confirmed'=>$pConfirm,
         'unconfirmed'=>$pUnconfirmed];
  }

  private function getBuyingStatistics($book_id){
      $hard = $this->CI->OrderedItemModel->getBookSalesCount($book_id);
      $ebook = $this->CI->EbookModel->getBookSalesCount($book_id);
      $read = $this->CI->ReadModel->getBookSalesCount($book_id);

      return ['ebook'=>$ebook,'hardcopy'=>$hard,'readings'=>$read];
  }

  public function getTotalCustomers(){
        return $this->CI->CustomerModel->getTotalCustomers();
  }
  public function getTotalReadingsSales(){
      return  $this->CI->ReadModel->getTotalSales();
  }

  public function getTotalHardCopySales(){
    return $this->CI->OrdersModel->getTotalSales();
  }

  public function getTotalEbookSales(){
      return $this->CI->EbookModel->getTotalSales();
  }

  private function genDates($limit = 30){
      $date = date_add(date_create(date('Y-m-d')),date_interval_create_from_date_string('1 day'));        //date_create(date('Y-m-d'));
      $datesArray = [];

      for($i=1; $i<=$limit;$i++){
          date_sub($date, date_interval_create_from_date_string('1 day'));
          array_push($datesArray, date_format($date, 'Y-m-d'));
      }
      return $datesArray;
  }

  private function getValue($value){
      return 0+($value?$value:0);
  }
}