<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AccountActivator
{
    private $CI;
   public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model(['ActivationModel']);
    }

    public function sendActivationEmail($email){
        $token = $this->genToken();
        $customer = $this->CI->CustomerModel->getCustomer(['email'=>$email]);
        $this->CI->ActivationModel->setToken($customer->customer_id,$token);
       return $this->sendEMail($email,$token);
    }


    private function sendEMail($email,$hash){
        $config['protocol']='smtp';
        $config['smtp_host'] = "smtpout.secureserver.net";
        $config['smtp_user'] = "info@researchclue.com.ng";
        $config['smtp_pass'] = "Active5544@";
        $config['smtp_port'] = '3535';
        $config['mailtype'] = 'html';
        $config['charset'] = "utf-8";
        $config['newline'] = "\r\n";

        $this->CI->email->initialize($config);

        $this->CI->email->from('info@researchclue.com.ng', 'Book Store');
        $this->CI->email->subject("BookStore | Activation Email");
        $this->CI->email->to($email);
        $this->CI->email->message('
            
     <h1>Welcome Aniefon Umanah</h1>
     <p class="text-muted">Your registration was successful click on the link below to activate your account</p>
     <div class="text-center">
     <a href="'.site_url('account/activate?ref='.$hash).'" class="btn btn-primary">Activate Account</a>
     <a href="'.site_url('account/activate?ref='.$hash).'"  class="btn btn-primary">'.site_url('account/activate?ref='.$hash).'</a>

        ');
        return $this->CI->email->send();


    }

    public function genToken(){
        return md5(rand());
    }

}