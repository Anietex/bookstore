<?php

class CodeGenerator
{
    private static  function  genCode(){
        $code=[];
       for($i =0;$i<8; $i++){
           $code[$i]=mt_rand(0,9);
       }
       return implode($code);
    }

    public  static function getCode(){
        $code = self::genCode();
        return $code;
    }




}