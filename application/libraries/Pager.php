<?php

class Pager
{

    private  $CI;
    private $pages_link;
    public function __construct($params){

        $this->tCont=$params['total_content'];
        $this->nLinks = $params['no_of_links'];
        $this->perPage = $params['content_per_page'];
    }


    public function setLink($link){
        $this->pages_link = $link;
    }

    public function getLinks(){
        $links = '';

        if($this->getTotalLinks()>1){
            $links .= '<ul class="pagination">';
            $prevPage = $this->getPrevPage();
            if($prevPage){
                $links .='
                <li class="page-item">
                    <a class="page-link" href="'.$this->pages_link.'='.$prevPage.'" aria-label="Previous">
                       <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            ';
            }
            $first = $this->getFirstPage();
            $allLinks = $this->getTotalLinks();
            $end=$this->nLinks;
            if($allLinks > 1){
                if($end > $allLinks){
                    $end=$allLinks;
                }

                for($i=1; $i<=$end; $i++){
                    $first+=1;
                    if($this->getActivePage()==0 && $first ==1){
                        if($first=1)
                            $links.='<li class="page-item active"><a class="page-link" href="'.$this->pages_link.'='.$first.'">'.$first.'</a></li>';
                    }else{
                        if($this->getActivePage() ==$first){
                            $links.='<li class="page-item active"><a class="page-link " href="'.$this->pages_link.'='.$first.'">'.$first.'</a></li>';

                        }else{
                            $links.='<li class="page-item"><a class="page-link" href="'.$this->pages_link.'='.$first.'">'.$first.'</a></li>';

                        }
                    }
                }
            }




            $nextPage = $this->getNextPage();
            if($nextPage){
                $links.='<li class="page-item">
                            <a class="page-link" href="'.$this->pages_link.'='.$nextPage.'" aria-label="Next">
                               <span aria-hidden="true">&raquo;</span>
                               <span class="sr-only">Next</span>
                            </a>
                          </li>';
            }
            $links.='</ul>';
        }

        return $links;


    }


    public function getTotalLinks(){

        return round($this->tCont/$this->perPage);
    }

    private function getActivePage(){
        return (isset($_GET['page'])&& is_numeric($_GET['page']))?$_GET['page']:0;

    }
    private function getFirstPage(){
        if(isset($_GET['page'])&& is_numeric($_GET['page'])){
            $currentPage = $this->getActivePage();
            if($currentPage >= $this->nLinks){
                if($this->getActivePage()>=$this->getTotalLinks()){
                    return $currentPage-$this->nLinks;

                }


                return $currentPage-$this->nLinks+1;



            }
        }

        return 0;
    }

    private function getPrevPage(){
        if($this->getActivePage()>1){
            return $this->getActivePage()-1;
        }
        return false;
    }

    private function getNextPage(){
        if($this->getActivePage()<$this->getTotalLinks()){
            return $this->getActivePage()+1;
        }
        return false;
    }
}