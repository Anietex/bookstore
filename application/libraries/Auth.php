<?php

class Auth
{
    private $CI;
    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->model('CustomerModel');
        $this->CI->load->helper(['url_helper']);
    }


    public function accountIsActivated(){
        $cus = $this->CI->CustomerModel->getCustomer(['customer_id'=>$_SESSION['customer_id']]);
        return $cus->activated == true;
    }

    public static function isLoggedIn(){
        return isset($_SESSION['customer_id']);
    }



}