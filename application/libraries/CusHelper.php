<?php

class CusHelper
{
    public static function genQty($max,$selected){
        for($i=1;$i<=$max;$i++){
            if($i==$selected)
                print '<option value="'.$i.'" selected>'.$i.'</option>';
            else
                print '<option value="'.$i.'">'.$i.'</option>';
        }
    }


    public static function forceDownload($file,$name){
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$name");
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: binary");

        readfile($file);
        exit;
    }

    public static  function alreadyDownloaded($book_id,$cus_id){
        $CI =& get_instance();
        $CI->load->model('EbookModel');

        $book = $CI->EbookModel->getEbook(['book_id'=>$book_id,'customer_id'=>$cus_id]);

        return $book?true:false;
    }





}