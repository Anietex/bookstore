<?php


class CartManager
{
    private  $items = [];
    public function __construct()
    {
        if(isset($_COOKIE['cart'])){
            $_SESSION['cart'] = json_decode($_COOKIE['cart'],true);
        }else{
           $_SESSION['cart'] = $this->items;
        }
    }

    public  function addItem($id,$item){

        $this->items = $_SESSION['cart'];
        if(array_key_exists($id, $this->items)){
            $this->updateQty($id,$_SESSION['cart'][$id]['qty']+1);
        }else{
            $this->items[$id] = $item;
            $_SESSION['cart'] = $this->items;
            setcookie('cart',json_encode($_SESSION['cart']),time()+60*60*24*7,'/');
        }

    }
    public function updateQty($id,$qty){
        $_SESSION['cart'][$id]['qty'] = $qty;
        setcookie('cart',json_encode($_SESSION['cart']),time()+60*60*24*7,'/');
    }

    public function getItems(){
        $CI =& get_instance();
        $CI->load->model('BookModel');
        $items = [];

        array_map(function ($item)use (&$items,$CI){
            $book = $CI->BookModel->getBook($item['id']);
            $data = [
                'id'=>$item['id'],
                'title'=>$book->title,
                'copies'=>$book->copies,
                'cover'=>$book->cover_photo_file,
                'price'=>$book->hardcopy_price,
                'subtotal' =>$item['qty']*$book->hardcopy_price,
                'qty' =>$item['qty']];
            array_push($items,$data);
        },$_SESSION['cart']);
        return $items;
    }

    public function remove($id){
        unset( $_SESSION['cart'][$id]);
        setcookie('cart',json_encode($_SESSION['cart']),time()+60*60*24*7,'/');

    }

    public function getTotalCost(){
        $items = $this->getItems();
        $cost = 0;
        foreach ($items as $item) {
            $cost+=$item['subtotal'];
        }

        return $cost;
    }

    public static function getTotalItems(){
        $cm = new CartManager();
        $items = $cm->getItems();
        $total = 0;
        foreach ($items as $item) {
            $total+=$item['qty'];
        }

        return $total;
    }

    public static function clearCart(){
        $_SESSION['cart'] = [];
        setcookie('cart',json_encode($_SESSION['cart']),time()-500,'/');
    }


}