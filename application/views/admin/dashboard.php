
<main class="co">
    <div class="page-title">
        <h3>Dashboard</h3>
    </div>
    <div class="summaries">
        <div class="row">
            <div class="col-sm-3">
                <div class="summary">
                    <div class="row no-gutters">
                        <div class="col-5 icon">
                            <span class="fa fa-4x fa-shopping-cart"></span>
                        </div>
                        <div class="col-7 text">
                            <p class="title">Hard Copy sales Revenue</p>
                            <h3 class="value">&#x20a6;<?=number_format($hardRev)?></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="summary">
                    <div class="row no-gutters">
                        <div class="col-5 icon ebook">
                            <span class="fa fa-4x fa-file-pdf-o"></span>
                        </div>
                        <div class="col-7 text">
                            <p class="title">Ebook sales Revenue</p>
                            <h3 class="value">&#x20a6;<?=number_format($ebookRev)?></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="summary">
                    <div class="row no-gutters">
                        <div class="col-5 icon reading">
                            <span class="fa fa-4x fa-book"></span>
                        </div>
                        <div class="col-7 text">
                            <p class="title">Online Readings sales revenue</p>
                            <h3 class="value">&#x20a6;<?=number_format($readRev)?></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="summary">
                    <div class="row no-gutters">
                        <div class="col-5 icon customers">
                            <span class="fa fa-4x fa-users"></span>
                        </div>
                        <div class="col-7 text">
                            <p class="title">Total Registered Customers</p>
                            <h3 class="value"><?=number_format($totalCustomers)?></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="circles">
        <div class="row">
            <div class="col-sm-3 ml-auto col-6">
                <div class="wrapper">
                    <div id="total-orders" data-percent="<?=$completed?>"></div>
                </div>
            </div>
            <div class="col-sm-3 m-auto col-6">
                <div class="wrapper">
                    <div id="confirm-orders" data-percent="<?=$confirmed?>"></div>
                </div>
            </div>
            <div class="col-sm-3 mr-auto col-6 mt-3">
                <div class="wrapper">
                    <div id="unconfirm-orders" data-percent="<?=$unconfirmed?>"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-4 p-4 ">
          <div class="chart bg-light">
                    <div id="container" style="width:100%; height:400px;"></div>
          </div>
    </div>

</main>