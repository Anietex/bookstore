<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/font-awesome.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/DataTables/datatables.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/jquery.circliful.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/dashboard.css')?>">
    <title>Book Store | Admin | Login </title>
</head>
<body>
<header>
    <div class="nav-wrapper">
        <div class="row">
            <div class="col-12 col-sm-3">
                <div class="logo">
                    BookStore
                </div>
            </div>
            <div class="col-sm-9 col-12">
            </div>
        </div>
    </div>
</header>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<main class="sign-in">
    <div class="form-wrapper sign-up my-5">

        <div class="row">

            <div class="col-sm-5 m-auto">
                <?=$msg->display()?>
                <div class="card">
                    <form method="post" action="<?=site_url('admin/login/')?>"  data-parsley-validate>
                        <h4 class="card-header">Admin Sign In</h4>
                        <div class="card-body">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
