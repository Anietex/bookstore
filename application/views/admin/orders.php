<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
    <div class="page-title">
        <h3>All Customer Orders</h3>
    </div>
    <?=$msg->display()?>
    <div class="orders">
        <table class="display hover table table-sm table-striped compact row-border" style="width:100%"  id="orders-table">
            <thead>
                <tr>
                    <th>S/N</th>
                    <th>Date</th>
                    <th>Customer Name</th>
                    <th>Amount Payed</th>
                    <th>Tracking Code</th>
                    <th>Transaction Ref</th>
                    <th>Details</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php $i=1; foreach ($orders as $order):?>
                <tr>
                    <td><?=$i++?></td>
                    <td><?=$order->order_date?></td>
                    <td><?=$order->first_name.' '.$order->last_name?></td>
                    <td>N<?=number_format($order->amount_payed)?></td>
                    <td><?=$order->transaction_ref?></td>
                    <td>TK<?=str_pad($order->order_id,5,'0',0)?></td>
                    <td><a href="<?=site_url('admin/order_details/'.$order->order_id)?>">View details</a></td>
                    <?php if($order->confirmed==0):?>
                    <td><a href="<?=site_url('admin/order/confirm/'.$order->order_id)?>">Confirm Order</a></td>
                     <?php else:?>
                        <?php if($order->completed==0):?>
                            <td><a href="<?=site_url('admin/order/complete/'.$order->order_id)?>">Complete Order</a></td>
                        <?php else:?>
                            <td class="text-primary">Completed</td>
                        <?php endif;?>
                    <?php endif;?>
                    </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>

</main>