<main>
    <div class="page-title">
        <h3>Manage Books</h3>
    </div>
<?=$msg->display()?>
    <div>
        <table class="display hover table table-sm table-striped compact row-border table-responsive" style="width:100%" id="books-table">
            <thead>
                <tr>
                    <th>S/N</th>
                    <th>Book title</th>
                    <th>Author</th>
                    <th>Category</th>
                    <th>Available Copies</th>
                    <th>Total Sales</th>
                    <th>Details</th>
                    <th class="text-center">Action</th>
                    <th></th>

                </tr>
            </thead>

            <tbody>
            <?php $i=1; foreach ($books as $book):?>
                <tr>
                    <td><?=$i++?></td>
                    <td><?=$book->title?></td>
                    <td><?=$book->author?></td>
                    <td><?=$book->cat_name?></td>
                    <td><?=$book->copies?></td>
                    <td><?=$book->sales?></td>
                    <td><a href="<?=site_url('admin/book_details/'.$book->book_id)?>">View</a> </td>
                    <td><a href="<?=site_url('book/edit/'.$book->book_id)?>">Edit</a> </td>
                    <td><button data-id="<?=$book->book_id?>" class="btn-link btn"  data-toggle="modal" data-target="#delete-modal"> Delete</button></td>

                </tr>
            <?php endforeach;
            ?>
            </tbody>
        </table>
    </div>


    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form id="delete-form" action="<?=site_url('book/delete/')?>" method="post">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Delete Options</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="book_and_orders" name="delete-option" id="customers">
                            <label class="form-check-label" for="customers">
                                Delete book record and customer orders
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="only_book" name="delete-option" id="only-books" checked>
                            <label class="form-check-label" for="only-books">
                               Delete only book record
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Continue</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
<!--<a href="--><?//=site_url('book/delete/'.$book->book_id)?><!--">Delete</a>-->