<main>
    <div class="page-title">
        <h3>Online Reading sales</h3>
    </div>
    <?=$msg->display()?>
    <div>
        <table class="display hover table table-sm table-striped compact row-border table-responsive" style="width:100%" id="books-table">
            <thead>
            <tr>
                <th>S/N</th>
                <th>Date</th>
                <th>Book title</th>
                <th>Customer Name</th>
                <th>Transaction Ref</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            <?php $i=1; foreach ($books as $book):?>
                <tr>
                    <td><?=$i++?></td>
                    <td><?=$book->date_sold?></td>
                    <td><?=$book->title?></td>
                    <td><?=$book->first_name.' '.$book->last_name?></td>
                    <td><?=$book->trans_ref?></td>
                    <td>&#x20a6;<?=number_format($book->amount_payed)?></td>
                </tr>
            <?php endforeach;
            ?>
            </tbody>
        </table>
    </div>
</main>
