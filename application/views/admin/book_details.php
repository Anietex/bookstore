<main>
    <div class="page-title">
        <h3>Book Details</h3>
    </div>
    <div class="meta">
        <div class="row">
            <div class="col-sm-3">
                <div class="book-cover">
                    <img src="<?=site_url('uploads/covers/'.$book->cover_photo_file)?>" class="img-fluid">
                </div>
            </div>
            <div class="col-sm-9">
                <div class="details">
                    <p><strong>Title:</strong> <?=$book->title?></p>
                    <p><strong>Author:</strong> <?=$book->author?></p>
                    <p><strong>Edition:</strong> <?=$book->edition?></p>
                    <p><strong>ISBN:</strong> <?=$book->isbn?></p>
                    <p><strong>Category:</strong> <?=$book->cat_name?></p>
                    <p><strong>Year:</strong> <?=$book->year_published?></p>
                    <p><strong>Pages:</strong> <?=$book->pages?></p>
                    <p><strong>Total Copies Sold:</strong> <?=$book->sales?></p>
                    <p><strong>Available Copies:</strong> <?=$book->copies?> copie(s)</p>
                    <p><strong>Price of Hard Copy:</strong> &#x20a6;<?=number_format($book->hardcopy_price)?></p>
                    <p><strong>Price of Downloading:</strong> &#x20a6;<?=number_format($book->ebook_price)?></p>
                    <p><strong>Price of Reading Online:</strong> &#x20a6;<?=number_format($book->read_price)?></p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="page-title">
        <h5>Sales Statistics</h5>
    </div>
    <div class="charts">
        <div class="row">
            <div class="col-sm-8">
                <div class="area-chart">
                    <div id="sales-stat" data-url="<?=site_url('api/book_stat_json/'.$book->book_id)?>">
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div id="buying-pie" data-url="<?=site_url('api/book_stat_json/'.$book->book_id)?>">

                </div>
            </div>
        </div>
    </div>
</main>
