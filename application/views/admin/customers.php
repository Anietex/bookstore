<main>
    <div class="page-title">
        <h3>Customers</h3>
    </div>
    <?=$msg->display()?>
    <div>
        <table class="display hover table table-sm table-striped compact row-border table-responsive" style="width:100%" id="books-table">
            <thead>
            <tr>
                <th>Customer Id</th>
                <th>Name</th>
                <th>Phone Number</th>
                <th>Email</th>
                <th>Address</th>
                <th class="text-center">Transaction History</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($customers as $customer):?>
                <tr>
                    <td><?=$customer->customer_id?></td>
                    <td><?=$customer->first_name.' '.$customer->last_name?></td>
                    <td><?=$customer->phone_no?></td>
                    <td><?=$customer->email?></td>
                    <td><?=$customer->address?></td>
                    <td class="text-center"><a href="<?=site_url('admin/customer/order_history/'.$customer->customer_id)?>">View</a> </td>
                </tr>
            <?php endforeach;
            ?>
            </tbody>
        </table>
    </div>
</main>
