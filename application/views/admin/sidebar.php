
<aside id="side-bar">
    <div class="sidebar" >
        <ul class="list-unstyled">
            <li ><a href="<?=site_url('admin')?>"><span class="fa fa-dashboard"> </span> Dashboard</a>  </li>
            <li><a href="<?=site_url('admin/book')?>" ><span class="fa fa-pencil"> </span> Add new book</a> </li>
            <li><a href="<?=site_url('admin/manage_books')?>"><span class="fa fa-edit"> </span>Manage Books </a> </li>
            <li><a href="<?=site_url('admin/categories')?>"><span class="fa fa-dedent"> </span>Manage Categories</a></li>
            <li ><a href="#"  id="dropdown-btn"><span class="fa fa-briefcase"> </span> Orders<span class="fa fa-chevron-left float-right" id="d-icon"></span> </a>
                <ul class="list-unstyled" id="dropdown">
                    <li><a href="<?=site_url('admin/orders')?>"> All Orders</a></li>
                    <li><a href="<?=site_url('admin/unconfirmed_orders')?>"> Unconfirmed Orders</a></li>
                    <li><a href="<?=site_url('admin/confirmed_orders')?>"> Confirmed Orders</a></li>
                    <li><a href="<?=site_url('admin/completed_orders')?>"> Completed Orders</a></li>
                </ul>
            </li>
            <li><a href="<?=site_url('admin/ebook_sales')?>"><span class="fa fa-file-pdf-o"> </span> e-Book Sales</a></li>
            <li><a href="<?=site_url('admin/online_reading')?>"><span class="fa fa-book"> </span>Online Reading Sales</a></li>
            <li><a href="<?=site_url('admin/customers')?>"><span class="fa fa-users"> </span> Customers</a></li>
        </ul>
    </div>
</aside>