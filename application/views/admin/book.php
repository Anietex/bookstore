<main>
    <div class="page-title">
        <h3><?=FlashData::get('page_title','Add new book')?></h3>
    </div>
    <?=$msg->display()?>
    <div class="book-form">
        <form enctype="multipart/form-data" action="<?=site_url('book/'.FlashData::get('action_page','create'))?>" method="post">
            <div class="row m-2">
                <div class="col-sm-9 border bg-light p-3">
                    <div class="left">
                        <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="book_title">Book title</label>
                                    <input type="text"  value="<?=FlashData::get('title')?>" class="form-control" id="book_title" name="book_title" placeholder="Book title" required>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label for="author">Author</label>
                                    <input type="text" value="<?=FlashData::get('author')?>" class="form-control" id="author" name="author" placeholder="Author" required>
                                </div>
                            <div class="form-group col-sm-4">
                                <label for="edition">Edition</label>
                                <input type="text" value="<?=FlashData::get('edition')?>" class="form-control" id="edition" name="edition" placeholder="Edition" required>
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="isbn">ISBN</label>
                                <input type="text" value="<?=FlashData::get('isbn')?>" class="form-control" id="isbn" name="isbn" placeholder="ISBN" required>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="year">Year Published</label>
                                    <input type="number" value="<?=FlashData::get('year')?>" class="form-control" id="year" name="year" placeholder="Year Published" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="pages">No of Pages</label>
                                    <input type="number" value="<?=FlashData::get('pages')?>" class="form-control" id="pages" name="pages" placeholder="No of Pages" required>
                                </div>
                            </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="copies">Available Hard Copies</label>
                                        <input type="number" value="<?=FlashData::get('copies')?>" class="form-control" id="copies" name="copies" placeholder="Available Hard Copies" required>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="hardcopy_price">Price of Hard copy</label>
                                    <input type="text" value="<?=FlashData::get('hardcopy')?>" class="form-control" id="hardcopy_price" name="hardcopy_price" placeholder="Price of hard copy" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="year">Price of e-book</label>
                                    <input type="number" value="<?=FlashData::get('ebook')?>" class="form-control" id="ebook_price" name="ebook_price" placeholder="Price of e-book" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="read_price">Price of Read Online</label>
                                    <input type="number" value="<?=FlashData::get('read')?>" class="form-control" id="read_price" name="read_price" placeholder="Price of Read Online" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" id="description" class="form-control" rows="8" placeholder="Enter book description here"><?=FlashData::get('description')?></textarea>
                        </div>
                    </div>

                </div>
                <div class="col-sm-3">
                    <div class="right bg-light p-3">
                        <div class="actions">
                            <button type="submit" name="action" value="add" class="btn btn-primary btn-block"><?=FlashData::get('btnText','Add')?></button>
                            <?php if(FlashData::get('show_btn')):?>
                            <button type="button" name="action" value="add" class="btn btn-danger btn-block" onclick="location.reload()">Cancel</button>
                            <?php endif;?>
                        </div>
                        <hr>
                        <div class="uploads">
                            <div class="form-group">
                                <label>Category</label>
                                <select class="form-control" name="category"  required>
                                    <option value="">Select category</option>
                                    <?php DashboardHelper::generateCategory(FlashData::get('category'))?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Upload Ebook</label>
                                <input type="file" class="form-control" name="ebook" <?=FlashData::get('ebook_required',"required='false'")?>>
                            </div>
                            <div class="form-group">
                                <label>Upload Book cover</label>
                                <input type="file" class="form-control" name="book_cover" id="cover-upload" <?=FlashData::get('cover_required',"required")?>>
                            </div>
                            <hr>
                            <div class="book-cover">
                                <img src="<?=FlashData::get('cover','../assets/images/cover_ph.png')?>" class="img-fluid" id="cover_photo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
</main>
<script type="text/javascript" src="<?=site_url('assets/js/tinymce/tinymce.min.js')?>"></script>
<script type="text/javascript">
    tinymce.init({
        selector: '#description',
        menubar:false,
        statusbar: false
    });
</script>