<main>
        <div class="page-title">
            <h4>Account Update</h4>
        </div>
        <div class="row ">
            <div class="col-sm-6 m-auto bg-light p-4">
                <?=$msg->display()?>
                    <form method="post" action="<?=site_url('admin/account_update/')?>"  data-parsley-validate>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" name="username" placeholder="Username" value="<?=$_SESSION['username']?>" required>
                            </div>
                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" class="form-control" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label>Old Password</label>
                                <input type="password" class="form-control" name="old-password" placeholder="Password" required>
                            </div>
                            <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
        </div>
</main>