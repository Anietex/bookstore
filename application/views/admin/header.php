<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/font-awesome.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/DataTables/datatables.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/jquery.circliful.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/dashboard.css')?>">
    <title>Book Store<?=isset($title)?' | '.$title:''?></title>
</head>
<body>
<header>
    <div class="nav-wrapper">
        <div class="row">
            <div class="col-7 col-sm-3">
                <div class="logo">
                    BookStore
                </div>
            </div>
            <div class="col-1 col-sm-1">
               <div id="sidebar-toggle" class="toggle-btn">
                   <span class="fa fa-bars fa-2x text-white"></span>
               </div>
            </div>
            <div class="col-sm-8 col-md-8 col-4">
                <nav>
                   <div class="user d-flex float-right dropdown">
                       <div class="icon">
                        <span class="fa fa-user"></span>
                       </div>
                       <div class="name">
                           <div class="dropdown">
                               <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   <span class="sm-hide"><?=$_SESSION['username']?></span>
                               </button>
                               <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                   <a class="dropdown-item" href="<?=site_url('admin/edit_account')?>">Edit Account</a>
                                   <a class="dropdown-item" href="<?=site_url('admin/sign_out')?>">Sign Out</a>
                               </div>
                           </div>
                       </div>
                   </div>
                </nav>
            </div>
        </div>
    </div>
</header>