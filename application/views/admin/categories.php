<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
    <div class="page-title">
        <h3>Manage Categories</h3>
    </div>
    <?=$msg->display()?>
    <div class="row">
        <div class="col-sm-8">
            <div class="form">
                <form method="post" action="<?=site_url('category/'.FlashData::get('action','create'))?>" class="">
                    <input type="hidden" name="cat_id" value="<?=FlashData::get('cat_id')?>">
                   <div class="row">
                       <div class="col-sm-8">
                           <div class="form-group">
                               <input class="form-control" value="<?=FlashData::get('cat_name')?>" name="cat_name" id="cat_name" placeholder="Category name" required>
                           </div>
                       </div>
                       <div class="col-sm-2">
                           <button class="btn btn-primary"><?=FlashData::get('btn-text',"Add")?></button>
                       </div>
                       <?php if(FlashData::get('editing')):?>
                       <div class="col-sm-1">
                           <a href="<?=site_url('admin/categories')?>" class="btn btn-danger">Cancel</a>
                       </div>
                       <?php endif;?>
                   </div>
                </form>
            </div>
            <div class="cat-table">
                <table class="table table-striped table-responsive">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Category Name</th>
                            <th colspan="2" class="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach ($categories as $category):?>
                        <tr>
                            <td><?=$i++?></td>
                            <td><?=$category->cat_name?></td>
                            <td><a href="<?=site_url('category/edit/'.$category->cat_id)?>">Edit</a> </td>
                            <td><a href="<?=site_url('category/delete/'.$category->cat_id)?>">Delete</a> </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</main>