<main>
    <div class="page-title">
        <h3>Transaction History of  <?=$customer_name?></h3>
    </div>
    <?=$msg->display()?>
    <div>

        <section class="my-4">
            <div class="page-title">
                <h5>Hard Copy Transactions</h5>
            </div>
            <div class="orders">
                <table class="table table-striped" style="width:100%">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Date</th>
                        <th>Amount Payed</th>
                        <th>Transaction Ref</th>
                        <th>Actions</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; foreach ($orders as $order):?>
                        <tr>
                            <td><?=$i++?></td>
                            <td><?=$order->order_date?></td>
                            <td>N<?=number_format($order->amount_payed)?></td>
                            <td><?=$order->transaction_ref?></td>
                            <td><a href="<?=site_url('admin/order_details/'.$order->order_id)?>">View details</a></td>
                            <?php if($order->confirmed==0):?>
                                <td><a href="<?=site_url('admin/order/confirm/'.$order->order_id)?>">Confirm Order</a></td>
                            <?php else:?>
                                <td class="text-primary">Confirmed</td>
                            <?php endif;?>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </section>
        <hr>
        <section class="my-4">
            <div class="page-title">
                <h5>Ebook Transactions</h5>
            </div>
            <div class="">
                <table class="table table-striped">
                   <thead>
                   <tr>
                       <th>S/N</th>
                       <th>Date</th>
                       <th>Book Title</th>
                       <th>Transaction Ref</th>
                   </tr>
                   </thead>
                    <tbody>
                        <?php $i=1; foreach($ebooks as $ebook):?>
                        <tr>
                            <td><?=$i++?></td>
                            <td><?=$ebook->date_sold?></td>
                            <td><?=$ebook->title?></td>
                            <td><?=$ebook->trans_id?></td>
                        </tr>
                          <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </section>
        <hr>
        <section class="my-4">
            <div class="page-title">
                <h5>Online Readings Transactions</h5>
            </div>
            <div class="">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>S/N</th>
                            <th>Date</th>
                            <th>Book Title</th>
                            <th>Transaction Ref</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i =1; foreach ($readings as $reading):?>
                    <tr>
                        <td><?=$i++?></td>
                        <td><?=$reading->date_sold?></td>
                        <td><?=$reading->title?></td>
                        <td><?=$reading->trans_ref?></td>
                    </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>

            </div>
        </section>
    </div>
</main>
