<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<main class="py-5">
    <div class="container py-3">
        <div class="checkout">
            <h3>Checkout</h3>
            <?php switch($prog):
             case "summary": ?>
                    <div class="summary">
                        <div class="p-wrapper">
                            <div class="prog">
                                <ul class="progress_bar">
                                    <li class="cart active">Your Cart Summary</li>
                                    <li class="ship">shipment Info</li>
                                    <li class="pay">Payment</li>
                                    <li class="complete">Complete</li>
                                </ul>
                            </div>
                        </div>
                        <hr class="m-1">
                        <h4 class="">Your Cart summary</h4>
                        <div class="cart">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Item</th>
                                    <th>Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($items as $item):?>
                                    <tr>
                                        <td class="text-primary"><?=$item['title']?></td>
                                        <td>&#x20a6;<?=number_format($item['subtotal'])?></td>
                                    </tr>
                                <?php endforeach;?>
                                <tr>
                                    <th>Grand Total</th>
                                    <th>&#x20a6;<?=number_format($total)?></th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center">
                            <hr>
                            <a href="<?=site_url('cart/checkout/shipping')?>" class="btn btn-primary">Continue <span class="fa fa-angle-double-right"></span></a>
                        </div>
                    </div>
            <?php break; case 'shipping':?>

                    <div class="shipping">
                        <div class="p-wrapper">
                            <div class="prog">
                                <ul class="progress_bar">
                                    <li class="cart active">Your Cart Summary</li>
                                    <li class="ship  active">shipment Info</li>
                                    <li class="pay">Payment</li>
                                    <li class="complete">Complete</li>
                                </ul>
                            </div>
                        </div>
                        <hr class="m-1">
                        <h4 class="">Shipment Information</h4>
                        <div class="ship">
                            <form action="<?=site_url('cart/update_shipping_info')?>" method="post">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label >Customer Name</label>
                                        <input type="text" class="form-control" name="customer_name" readonly value="<?=$_SESSION['customer_name']?>">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Shipping Address</label>
                                        <input type="text" name="address" class="form-control" value="<?=$_SESSION['address']?>">
                                        <small class="text-muted">You can change this info to another address</small>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" name="phone" value="<?=$_SESSION['phone']?>">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label >Email</label>
                                        <input type="text" class="form-control" name="email" value="<?=$_SESSION['email']?>">
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <button type="submit" class="btn btn-warning">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="text-center">
                            <hr>
                            <a href="<?=site_url('cart/checkout/summary')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Summary</a>
                            <a href="<?=site_url('cart/checkout/payment')?>" class="btn btn-primary">Continue <span class="fa fa-angle-double-right"></span></a>
                        </div>
                    </div>
            <?php break; case 'payment':?>
                    <div class="payment">
                        <div class="p-wrapper">
                            <div class="prog">
                                <ul class="progress_bar">
                                    <li class="cart active">Your Cart Summary</li>
                                    <li class="ship active">shipment Info</li>
                                    <li class="pay active">Payment</li>
                                    <li class="complete">Complete</li>
                                </ul>
                            </div>
                        </div>
                        <hr class="m-1">
                        <h4 class="">Payment</h4>
                        <hr>
                        <div class="cart text-center">
                            <?=$msg->display()?>
                            <img src="<?=site_url('assets/images/cards.png')?>" class="img-fluid" width="30%">
                            <p class="text-info">Confirm the following information and make payment</p>
                           <h5>Customer Name : <?=$_SESSION['customer_name']?></h5>
                            <h5>Shipping To: <?=$_SESSION['address']?></h5>
                            <h5>You will be Charged: &#x20a6;<?=number_format($total)?></h5>
                        </div>
                        <div class="text-center">
                            <hr>
                            <button
                                    data-cart_items='<?=json_encode($items)?>'
                                    data-customer_id="<?=$customer->customer_id?>"
                                    data-email="<?=$customer->email?>"
                                    data-phone_no="<?=$_SESSION['phone']?>"
                                    data-shipping_address="<?=$_SESSION['address']?>"
                                    data-amount="<?=$total*100?>"
                                    data-callback="<?=site_url('checkout/authpayment')?>"
                                    id="check-out-pay"
                                    href="<?=site_url('cart/checkout/complete')?>" class="btn btn-success btn-lg">Make Payment</button>
                            <br><br>
                            <a href="<?=site_url('cart/checkout/shipping')?>" class="btn btn-primary"><span class="fa fa-angle-double-left"></span> Shipment</a>

                            <script>
                            var data =<?=json_encode($items)?>
                        </script>
                        </div>
                    </div>
            <?php break; case 'complete':?>
                    <div class="complete">
                        <div class="p-wrapper">
                            <div class="prog">
                                <ul class="progress_bar">
                                    <li class="cart active">Your Cart Summary</li>
                                    <li class="ship active">shipment Info</li>
                                    <li class="pay active">Payment</li>
                                    <li class="complete active">Complete</li>
                                </ul>
                            </div>
                        </div>
                        <hr class="m-1">
                        <h4 class="">Transaction Completed</h4>
                        <hr>
                        <div class="cart px-3">
                            <h5 class="text-muted" >Congratulations, Transaction was successful</h5>
                            <p>You will get a confirmation  email once your order is confirm, and your product will be shipped to you</p>
                            <p><span class="font-weight-bold">Your Transaction Reference is:</span> <?=$trans_ref?></p>
                            <p><span class="font-weight-bold">Your order tracking code is:</span> <?=$order_id?></p>
                            <p>Thanks for doing business with us.</p>
                        </div>
                        <div class="text-center">
                            <hr>
                            <a href="<?=site_url('books')?>" class="btn btn-primary">Continue Shopping</a>
                        </div>
                    </div>
                <?php  default:?>
            <?php endswitch;?>

        </div>
    </div>
</main>
<script src="https://js.paystack.co/v1/inline.js"></script>