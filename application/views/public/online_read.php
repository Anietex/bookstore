<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="profile" >
    <h4>Online Readings</h4>
    <div class="border p-1 rounded">

        <?php if($readings):?>
        <?php foreach ($readings as $reading):?>
            <div class="row">
                <div class="col-sm-2 col-3">
                    <div class="cover">
                        <img src="<?=site_url('uploads/covers/'.$reading->cover_photo_file)?>" class="img-fluid" width="80px">
                    </div>
                </div>
                <div class="col-8 col-sm-10">
                    <p class="font-weight-normal"><a href="<?=site_url('book/details/'.$reading->book_id)?>"><?=$reading->title?></a> </p>
                    <div class="row">
                        <div class="col-sm-5">
                            <p><strong>Date Purchased:</strong> <?=$reading->date_sold?></p>
                        </div>
                        <div class="col-sm-3">
                            <a href="<?=site_url('reader/read/'.$reading->identifier)?>" class="btn btn-warning">Read Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        <?php endforeach;?>
        <?php else:?>
        <div class="text-center py-3">
            <h5 class="text-muted">You currents do not have any online readings</h5>
        </div>
        <?php endif;?>
    </div>
</section>
</div>
</div>
</div>
</div>
</main>
