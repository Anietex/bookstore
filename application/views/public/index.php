<div class="main">
    <section class="slides">
        <div class="slide-wrapper">
            <div class="overlay">
                <div class="swiper-container">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->

                        <?php foreach ($randomBooks as $rBook):?>
                        <div class="swiper-slide">
                            <div class="row">
                                <div class="col-sm-3 col-md-4 ml-auto">
                                    <div class="s-cover">
                                        <img src="<?=site_url('uploads/covers/'.$rBook->cover_photo_file)?>" class="img-fluid">
                                    </div>
                                </div>
                                <div class="col-sm-7 mr-auto col-md-4">
                                    <div class="s-desp">
                                        <h4 class="text-uppercase text-light text-center "><?=$rBook->title?></h4>
                                        <p class="text-light"><?=substr($rBook->description,0,300)?>
                                        </p>
                                        <div class="text-center">
                                            <a href="<?=site_url('book/details/'.$rBook->book_id)?>" class="btn btn-primary text-center">Buy Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach;?>


                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>

            </div>
            <ul id="slippry-bg">
                <li>
                    <a href="#slide1"><img src="assets/images/book1.jpg" class="img-fluid"></a>
                </li>
                <li>
                    <a href="#slide2"><img src="assets/images/book2.jpg" class="img-fluid"></a>
                </li>
                <li>
                    <a href="#slide3"><img src="assets/images/book3.jpg" class="img-fluid"></a>
                </li>
                <li>
                    <a href="#slide3"><img src="assets/images/book4.jpg" class="img-fluid"></a>
                </li>
            </ul>
        </div>
    </section>
    <section class="books-home container">
        <div class="popular panel">
            <div class="section-title">
                <h4>Popular Books</h4>
            </div>
            <div class="row">
                <?php foreach ($popularBooks as $pbook):?>


                <div class="col-sm-4 col-md-2  col-12">
                    <div class="book">
                        <div class="panel-front">
                            <div class="book-cover">
                                <img src="uploads/covers/<?=$pbook->cover_photo_file?>" class="img-fluid">
                            </div>
                            <div class="meta">
                                <p class="b-title"><a href="<?=site_url('book/details/'.$pbook->book_id)?>"><?=$pbook->title?></a> </p>
                                <hr class="p-0 m-0">
                                <h5 class="price">&#x20a6;<?=number_format($pbook->hardcopy_price)?></h5>
                            </div>
                        </div>
                        <div class="panel-overlay">
                            <p class="b-title"><?=$pbook->title?></p>
                            <p class="m-0"><?=$pbook->author?>e</p>
                            <p class="m-0"><?=$pbook->year_published?></p>
                            <p><?=$pbook->pages?> pages</p>
                            <?php if($pbook->copies>0):?>
                            <a href="<?=site_url('cart/additem/'.$pbook->book_id)?>" class="btn btn-primary btn-block">Add to Cart</a>
                           <?php endif; ?>
                            <?php if($pbook->ebook_file):?>
                            <a href="<?=site_url('ebook/buy/'.$pbook->book_id)?>" class="btn btn-info btn-block">Download</a>
                            <?php endif;?>
                            <a href="<?=site_url('book/details/'.$pbook->book_id)?>" class="btn btn-warning btn-block">View Details</a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
                </div>
        </div>
        <div class="new mt-4 panel">
            <div class="section-title">
                <h4>New Books</h4>
            </div>
            <div class="row">
                <?php foreach ($newBooks as $nbook):?>
                <div class="col-sm-4 col-md-2  col-12">
                    <div class="book">
                        <div class="panel-front">
                            <div class="book-cover">
                                <img src="uploads/covers/<?=$nbook->cover_photo_file?>" class="img-fluid">
                            </div>
                            <div class="meta">
                                <p class="b-title"><a href="<?=site_url('book/details/'.$nbook->book_id)?>"><?=$nbook->title?></a> </p>
                                <hr class="p-0 m-0">
                                <h5 class="price">&#x20a6;<?=number_format($nbook->hardcopy_price)?></h5>
                            </div>
                        </div>
                        <div class="panel-overlay">
                            <p class="b-title"><?=$nbook->title?></p>
                            <p class="m-0"><?=$nbook->author?></p>
                            <p class="m-0"><?=$nbook->year_published?></p>
                            <p><?=$nbook->pages?> pages</p>
                            <?php if($nbook->copies>0):?>
                            <a href="<?=site_url('cart/additem/'.$nbook->book_id)?>" class="btn btn-primary btn-block">Add to Cart</a>
                           <?php endif;?>
                            <?php if($nbook->ebook_file):?>
                            <a href="<?=site_url('ebook/buy/'.$nbook->book_id)?>" class="btn btn-info btn-block">Download</a>
                          <?php endif?>
                            <a href="<?=site_url('book/details/'.$nbook->book_id)?>" class="btn btn-warning btn-block">View Details</a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>



        <div class="all mt-4 panel">
            <div class="section-title">
                <h4>All Books</h4>
            </div>
            <div class="row">
                <?php foreach ($allBooks as $abook):?>
                <div class="col-sm-4 col-md-2  col-12">
                    <div class="book">
                        <div class="panel-front">
                            <div class="book-cover">
                                <img src="uploads/covers/<?=$abook->cover_photo_file?>" class="img-fluid">
                            </div>
                            <div class="meta">
                                <p class="b-title"><a href="<?=site_url('book/details/'.$abook->book_id)?>"><?=$abook->title?></a> </p>
                                <hr class="p-0 m-0">
                                <h5 class="price">&#x20a6;<?=number_format($abook->hardcopy_price)?></h5>
                            </div>
                        </div>
                        <div class="panel-overlay">
                            <p class="b-title"><?=$abook->title?></p>
                            <p class="m-0"><?=$abook->author?></p>
                            <p class="m-0"><?=$abook->year_published?></p>
                            <p><?=$abook->pages?> pages</p>
                            <?php if($abook->copies>0):?>
                            <a href="<?=site_url('cart/additem/'.$abook->book_id)?>" class="btn btn-primary btn-block">Add to Cart</a>
                          <?php endif;?>
                            <?php if($abook->ebook_file):?>
                            <a href="<?=site_url('ebook/buy/'.$abook->book_id)?>" class="btn btn-info btn-block">Download</a>
                           <?php endif;?>
                            <a href="<?=site_url('book/details/'.$abook->book_id)?>" class="btn btn-warning btn-block">View Details</a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>
            <div class="text-center mt-5">
                <a href="<?=site_url('books')?>" class="btn btn-warning more-btn">View More</a>
            </div>
            <hr>
        </div>
    </section>
</div>