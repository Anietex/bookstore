<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<main>
    <div class="form-wrapper sign-up my-5">

        <div class="row">
            <div class="col-sm-5 m-auto">
                <?=$msg->display()?>
                <div class="card">
                    <form method="post" action="<?=site_url('account/forget_password/')?>"  data-parsley-validate>
                        <h5 class="card-header">Forget Password</h5>
                        <div class="card-body">
                            <p class="text-muted">Enter your email below to receive a password reset link</p>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="email" placeholder="example@domain.com" required>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Reset Password</button><br>
                            <p class="d-inline">Already have an account <a href="<?=site_url('sign-in')?>">Sign In</a> </p>
                            <p class="text-muted d-inline">| Don't Have an account <a href="<?=site_url('sign-up')?>">Sign up Here</a> </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
