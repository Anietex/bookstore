<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
    <div class="">
        <div class="wrapper">
            <div class="row">
                <div class="col-sm-2">
                    <div class="books-sidebar">
                        <aside>
                            <ul class="list-unstyled">
                                <li><a href="<?=site_url('books')?>" >All Books</a></li>
                                <?php foreach ($categories as $category):?>
                                    <li><a href="<?=site_url('category/'.$category->cat_id.'/'.url_title($category->cat_name))?>"><?=$category->cat_name?></a> </li>
                                <?php endforeach;?>
                            </ul>
                        </aside>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="ection-title">
                        <h5>Seearch results of: "<?=htmlentities($_GET['query'])?>"</h5>
                    </div>
                    <?php if($books):?>
                    <div class="books-wrapper">
                        <div class="row">
                            <?php foreach ($books as $pbook):?>
                                <div class="col-sm-2">
                                    <div class="book">
                                        <div class="panel-front">
                                            <div class="book-cover">
                                                <img src="<?=site_url('uploads/covers/'.$pbook->cover_photo_file)?>" class="img-fluid">
                                            </div>
                                            <div class="meta">
                                                <p class="b-title"><a href="<?=site_url('book/details/'.$pbook->book_id)?>"><?=$pbook->title?></a> </p>
                                                <hr class="p-0 m-0">
                                                <h5 class="price">&#x20a6;<?=number_format($pbook->hardcopy_price)?></h5>
                                            </div>
                                        </div>
                                        <div class="panel-overlay">
                                            <p class="b-title"><?=$pbook->title?></p>
                                            <p class="m-0"><?=$pbook->author?></p>
                                            <p class="m-0"><?=$pbook->year_published?></p>
                                            <p><?=$pbook->pages?> pages</p>
                                            <?php if($pbook->copies>0):?>
                                                <a href="<?=site_url('cart/additem/'.$pbook->book_id)?>" class="btn btn-primary btn-block">Add to Cart</a>
                                            <?php endif;?>
                                            <?php if($pbook->ebook_file):?>
                                                <a href="<?=site_url('ebook/buy/'.$pbook->book_id)?>" class="btn btn-info btn-block">Download</a>
                                            <?php endif;?>
                                            <a href="<?=site_url('book/details/'.$pbook->book_id)?>" class="btn btn-warning btn-block">View Details</a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-3 m-auto">
                            <?=$pages?>
                        </div>

                    </div>
                </div>
            </div>
            <?php else:?>
                <div class="no-content">
                    <div class="text-center">
                        <h5 class="text-muted ">We can't find what you are looking form</h5>
                        <a href="<?=site_url('books')?>" class="btn btn-primary">View all Books</a>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
    </div>
</main>

