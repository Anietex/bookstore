<section class="profile" >
    <h4>Order Details</h4>
    <div class=" p-1 rounded">
        <div class="row">
            <div class="col-sm-4 border">
                <h6>Order Date: <span class="font-weight-normal"><?=$order->order_date?></span></h6>
            </div>
            <div class="col-sm-4 border">
                <h6>Order Email: <span class="font-weight-normal"><?=$order->email?></h6>
            </div>
            <div class="col-sm-4 border">
                <h6>Order Phone Number: <span class="font-weight-normal"><?=$order->phone_no?></span></h6>
            </div>
            <div class="col-sm-4 border">
                <h6>Shipping Address: <span class="font-weight-normal"><?=$order->shipping_address?></span></h6>
            </div>
            <div class="col-sm-4 border">
                <h6>Tracking Code: <span class="font-weight-normal">TK<?=str_pad($order->order_id,5,'0',0)?></span></h6>
            </div>
            <div class="col-sm-4 border">
                <h6>Transaction Ref: <span class="font-weight-normal"><?=$order->transaction_ref?></span></h6>
            </div>
        </div>
        <div class="text-center mt-3">
            <?php if($order->confirmed == 1):?>
            <h3><span class="badge badge-success">Confirmed</span> </h3>
            <?php else:?>
            <h3><span class="badge badge-danger">Unconfirmed</span> </h3>
            <?php endif;?>
        </div>


        <div class="mt-3">
            <h5>Ordered Books</h5>
            <div class="">
                <table class="table">
                    <tr>
                        <th>S/N</th>
                        <th>Book title</th>
                        <th>Copies</th>
                        <th>Cost per Copy</th>
                        <th>Total Cost</th>
                    </tr>
                    <?php $i=1; $total = 0; foreach ($books as $book): $total+=$book->cost; ?>
                    <tr>
                        <td><?=$i++?></td>
                        <td><a href="<?=site_url('book/details/'.$book->book_id)?>"> <?=$book->title?></a> </td>
                        <td><?=$book->quantity?></td>
                        <td>&#x20a6;<?=number_format($book->cost)?></td>
                        <td>&#x20a6;<?=number_format($book->total_cost)?> </td>
                    </tr>

                    <?php endforeach;?>
                    <tr>
                        <th colspan="4">Total</th>
                        <th>&#x20a6;<?=number_format($total)?></th>
                    </tr>
                </table>

            </div>
        </div>

    </div>
</section>
</div>
</div>
</div>
</div>
</main>