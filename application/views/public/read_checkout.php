<main class="py-5">
    <div class="container py-3">
        <div class="checkout">
            <h3 class="text-center">Pay and Read</h3>

            <?php switch ($prog):
                case "billing":?>
                    <div class="summary">
                        <div class="p-wrapper">
                            <div class="prog">
                                <ul class="progress_bar">
                                    <li class="ship active">Billing Info</li>
                                    <li class="pay">Payment</li>
                                    <li class="complete">Read</li>
                                </ul>
                            </div>
                        </div>
                        <hr class="m-1">
                        <h4 class="">Billing Info</h4>
                        <hr>
                        <div class="cart">
                            <p><span class="font-weight-bold">Customer Name:</span> <?=$customer->first_name.' '.$customer->last_name?></p>
                            <p><span class="font-weight-bold">Email:</span> <?=$customer->email?></p>
                            <p><span class="font-weight-bold">Book Title:</span> <?=$book->title?></p>
                            <p><span class="font-weight-bold">Amount:</span> &#x20a6;<?=number_format($book->read_price)?></p>
                        </div>
                        <div class="text-center">
                            <hr>
                            <a id="pay-btn" href="<?=site_url('read/buy/'.$book->book_id.'/payment')?>" class="btn btn-primary">Pay and Read</a>
                        </div>
                    </div>
                    <?php break; case "payment": ?>
                    <div class="payment">
                        <div class="p-wrapper">
                            <div class="prog">
                                <ul class="progress_bar">
                                    <li class="ship active">Billing Info</li>
                                    <li class="pay active">Payment</li>
                                    <li class="complete">Read</li>
                                </ul>
                            </div>
                        </div>
                        <div class="py-5 text-center">
                            <?=$msg->display()?>
                            <div class="py-3">
                                <img src="<?=site_url('assets/images/cards.png')?>" class="img-fluid" width="30%">
                            </div>
                            <button
                                    data-email="<?=$customer->email?>"
                                    data-amount="<?=$book->read_price*100?>"
                                    data-book_id="<?=$book->book_id?>"
                                    data-customer_id="<?=$customer->customer_id?>"
                                    data-callback="<?=site_url('read/authpayment')?>"
                                    id="pay-btn"
                                    type="button" class="btn btn-success btn-lg">Pay &#x20a6;<?=number_format($book->read_price)?></button>
                        </div>
                    </div>
                    <?php break;case "read":?>
                    <div class="download">
                        <div class="p-wrapper">
                            <div class="prog">
                                <ul class="progress_bar">
                                    <li class="ship active">Billing Info</li>
                                    <li class="pay active">Payment</li>
                                    <li class="complete active">Read</li>
                                </ul>
                            </div>
                        </div>
                        <hr>
                        <div class="text-center">
                            <p class="">Congratulation your payment was successful</p>
                            <p>Click on the button below to start reading your e-book</p>
                            <a href="<?=site_url('reader/read/'.$urid)?>" class="btn btn-primary">Start Reading</a>
                        </div>
                    </div>
                <?php endswitch;?>

        </div>
    </div>
</main>
<script src="https://js.paystack.co/v1/inline.js"></script>


