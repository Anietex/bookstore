<footer>
    <div class="footer-wrapper container">
        <div class="row">
            <div class="col-sm-6">
                <div class="links">
                    <h5>Contact Us</h5>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Enter Message"></textarea>
                        </div>
                        <button type="button" class="btn btn-warning">Send</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="links">
                    <h5>Links</h5>

                </div>
            </div>
            <div class="col-sm-3">
                <div class="links">
                    <h5>Socials</h5>

                </div>
            </div>
        </div>
    </div>
</footer>


<script type="text/javascript" src="<?=site_url("assets/js/jquery-3.3.1.min.js")?>"></script>
<script type="text/javascript" src="<?=site_url("assets/js/popper.min.js")?>"></script>
<script type="text/javascript" src="<?=site_url("assets/js/bootstrap.min.js")?>"></script>
<script type="text/javascript" src="<?=site_url("assets/js/slippry.min.js")?>"></script>
<script type="text/javascript" src="<?=site_url("assets/js/swiper.js")?>"></script>
<script type="text/javascript" src="<?=site_url('assets/js/paystack.js')?>"></script>
<script type="text/javascript" src="<?=site_url('assets/js/parsley.min.js')?>"></script>
<script type="text/javascript" src="<?=site_url("assets/js/custom.js")?>"></script>

</body>
</html>