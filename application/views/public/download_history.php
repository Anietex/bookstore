<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="profile" >
    <h4>Download History</h4>
    <div class="border p-1 rounded">
        <?php if($ebooks):?>
        <?php foreach ($ebooks as $ebook):?>
        <div class="row">
            <div class="col-sm-2 col-3">
                <div class="cover">
                    <img src="/assets/images/covers/cover5.jpg" class="img-fluid" width="80px">
                </div>
            </div>
            <div class="col-8 col-sm-10">
                <p class="font-weight-normal"><a href=""><?=$ebook->title?></a> </p>
                <div class="row">
                    <div class="col-sm-5">
                        <p><strong>Date Purchased:</strong> <?=$ebook->date_sold?></p>
                    </div>
                    <div class="col-sm-4">
                        <p><strong>Downloaded:</strong> <?=$ebook->download_count?> time(s)</p>
                    </div>
                    <div class="col-sm-3">
                        <a href="<?=site_url('ebook/download/'.$ebook->download_id)?>" class="btn btn-warning">Download</a>
                    </div>
                </div>
            </div>
        </div>
        <hr>
            <?php endforeach;?>
        <?php else:?>
            <div class="text-center py-3">
                <h5 class="text-muted">You currents do not have any download history</h5>
            </div>
        <?php endif;?>
    </div>
</section>
</div>
</div>
</div>
</div>
</main>
