<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<main>
    <div class="form-wrapper sign-up my-5">

        <div class="row">
            <div class="col-sm-5 m-auto">
                <div class="card">
                    <form method="post" action="<?=site_url('account/password_reset/')?>"  data-parsley-validate>
                        <h5 class="card-header">New Password</h5>
                       <input type="hidden" name="id" value="<?=$id?>">

                        <div class="card-body">
                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" class="form-control" name="password" id="password"  required>
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" name="c-password" data-parsley-equalto="#password"  data-parsley-equalto-message="Password do not match" required>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update Password</button><br>
                            <p class="d-inline">Already have an account <a href="<?=site_url('sign-in')?>">Sign In</a> </p>
                            <p class="text-muted d-inline">| Don't Have an account <a href="<?=site_url('sign-up')?>">Sign up Here</a> </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
