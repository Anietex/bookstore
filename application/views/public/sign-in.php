<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<main>
    <div class="form-wrapper sign-up my-5">

        <div class="row">
            <div class="col-sm-5 m-auto">
                <?=$msg->display()?>
                <div class="card">
                    <?php $ref = isset($_GET['ref'])?'?ref='.$_GET['ref']:'' ?>
                    <form method="post" action="<?=site_url('account/login/'.$ref)?>"  data-parsley-validate>
                        <h4 class="card-header">Sign In</h4>
                        <div class="card-body">
                                <div class="form-group">
                                    <label>Email or Phone Number</label>
                                    <input type="text" class="form-control" name="username" placeholder="Email or Phone number" required>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password" required>
                                </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="remember" type="checkbox" value="true">
                                    Remember me
                                </label>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Sign in</button>
                            <p class="d-inline"><a href="<?=site_url('forget-password')?>">Forget password</a> </p>
                            <p class="text-muted d-inline">| Don't Have an account <a href="<?=site_url('sign-up')?>">Sign up Here</a> </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
