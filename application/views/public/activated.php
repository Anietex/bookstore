<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
    <div class="container">
        <div class="reg-success py-5 mb-3">
            <h2>Account Activated</h2>
            <?php if(FlashData::get('alreadyActivated',false)===true):?>
            <p>Your account has already been activated proceed to sign in</p>
            <?php else:?>
            <p>Your account has  been successfully activated, proceed to sign in</p>
            <?php endif;?>
<!--            <a href="--><?//=site_url('sign-in')?><!--" class="btn btn-primary">Sign in</a>-->
        </div>
    </div>
</main>
