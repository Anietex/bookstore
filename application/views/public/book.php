<main>
    <div class="container">
        <div class="book-wrapper">
            <div class="row">
                <div class="col-sm-3 ml-auto">
                    <div class="book-cover">
                        <img src="<?=site_url('uploads/covers/'.$book->cover_photo_file)?>" class="img-fluid">
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="meta">
                        <h4><?=$book->title?></h4>
                        <h6>Author: <?=$book->author?></h6>
                        <h6>Edition: <?=$book->edition?> Edition</h6>
                        <h6>ISBN: <?=$book->isbn?></h6>
                        <h6>Pages: <?=$book->pages?></h6>
                        <h6>Year Published: <?=$book->year_published?></h6>
                    </div>
                    <div  class="buy-option mt-5">
                        <div class="row">
                            <?php if($book->copies>0):?>
                            <div class="col-sm-4">
                                <div class="option border m-2">
                                    <h6>Buy Hard Copy</h6>
                                    <h2 class="price">&#x20a6;<?=number_format($book->hardcopy_price)?></h2>
                                    <a href="<?=site_url('cart/additem/'.$book->book_id)?>" role="button" class="btn btn-sm btn-primary btn-block">Add to Cart</a>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php if($book->ebook_file):?>
                            <div class="col-sm-4">
                                <div class="option border m-2">
                                    <h6>Buy Soft Copy</h6>
                                    <h2 class="price">&#x20a6;<?=number_format($book->ebook_price)?></h2>
                                    <a href="<?=site_url('ebook/buy/'.$book->book_id)?>" type="button" class="btn btn-info btn-sm btn-block">Download</a>
                                </div>

                            </div>
                            <div class="col-sm-4">
                                <div class="option border m-2">
                                    <h6>Pay and Read Online</h6>
                                    <h2 class="price">&#x20a6;<?=number_format($book->read_price)?></h2>
                                    <a href="<?=site_url('read/buy/'.$book->book_id.'/billing')?>" type="button" class="btn btn-warning btn-sm btn-block">Read Online</a>
                                </div>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="description">
                <div class="">
                    <h4>Description</h4>
                    <p><?=$book->description?></p>
                </div>
            </div>
        </div>
    </div>
</main>
