<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/font-awesome.min.css')?>">
    <link rel="stylesheet" type="text/css" href="assets/css/slippry.css">
    <link rel="stylesheet" type="text/css" href="assets/css/swiper.css">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/smoke.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?=site_url('assets/css/style.css')?>">
    <title>Book Store<?=isset($title)?' | '.$title:''?></title>
</head>
<body>
<header>
    <div class="search-box" id="search-pane">
        <form action="<?=site_url('search')?>" method="get">
            <div class="">
                <input type="search"  value="<?=isset($_GET['query'])?$_GET['query']:''?>"   placeholder="Search for book title or book author" name="query" required>
                <button type="submit"><span class="fa fa-search"></span></button>
            </div>
        </form>
    </div>
    <nav class="row">
        <div class="col-sm-2 col-2">
            <div class="nav-btn text-right" id="big-pane-toggle">
                <span class="fa fa-bars icon"></span>
            </div>
        </div>
        <div class="col-sm-6 col-4">
            <div class="text-center logo">
               <a href="<?=site_url()?>"> BookStore</a>
            </div>
        </div>
        <div class="col-sm-2 col-4">
            <div class="row quick-action">
                <div class="col-4">
                    <span class="fa fa-search icon" id="search-toggle"></span>
                </div>
                <div class="col-4">
                    <a href="<?=site_url('cart')?>"><i class="fa fa-shopping-cart icon">  <span class="badge"><?=CartManager::getTotalItems()?></span></i></a>
                </div>
                <div class="col-4">
                    <div class="dropdown">
                        <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-user icon"></span><small><?=Auth::isLoggedIn()?$_SESSION['first_name']:''?></small>
                        </a>
                        <div class="dropdown-menu bg-primary" aria-labelledby="dropdownMenuLink">
                            <?php if(Auth::isLoggedIn()):?>
                            <a class="dropdown-item" href="<?=site_url('customer')?>">Edit Profile</a>
                            <a class="dropdown-item" href="<?=site_url('customer/online_read')?>">Online Readings</a>
                            <a class="dropdown-item" href="<?=site_url('customer/download_history')?>">Download History</a>
                            <a class="dropdown-item" href="<?=site_url('customer/order_history')?>">Orders</a>
                                <hr class="my-0">
                            <a class="dropdown-item" href="<?=site_url('account/logout')?>">Sign Out</a>
                            <?php else:?>
                            <a class="dropdown-item" href="<?=site_url('sign-up')?>">Sign Up</a>
                            <a class="dropdown-item" href="<?=site_url('sign-in')?>">Sign In</a>
                            <?php endif?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <nav class="nav-links" id="big-pane">
      <div class="container">
          <div class="links">
              <div class="row">
                  <div class="col-12 col-sm-3">
                      <a href="<?=site_url()?>">Home</a>
                  </div>
                  <div class="col-12 col-sm-3">
                      <a href="<?=site_url('books')?>">All Books </a>
                  </div>
                  <div class="col-12 col-sm-3">
                      <a href="<?=site_url('sign-up')?>">Sign Up</a>
                  </div>
                  <div class="col-12 col-sm-3">
                      <a href="<?=site_url('sign-in')?>">Sign In</a>
                  </div>
              </div>
          </div>
          <div class="wrapper mt-3">
              <div class="row">
                  <div class="col-sm-10 m-auto col-10">
                      <div class="categories">
                          <h5>Categories</h5>
                          <?php foreach ($categories as $category):?>
                          <div class="cat">
                              <a href="<?=site_url('category/'.$category->cat_id.'/'.url_title($category->cat_name))?>"><?=$category->cat_name?></a>
                          </div>
                          <?php endforeach;?>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </nav>
</header>