<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
    <div class="container">
        <div class="reg-success py-5 mb-3">
            <?=$msg->display();?>
            <h2>Welcome <?=FlashData::get('name')?></h2>
            <p>Your registration was successful</p>
            <p>An email has been sent to <span class="text-muted"><?=FlashData::get('email')?></span></p>
            <p>Check your email for an activation link in order to complete your registration</p>
            <div class="text-center">
                <p>You did not receive any email?  </p>
                <a href="<?=site_url('account/resend?to='.FlashData::get('send_email'))?>" class="btn btn-primary">Resend Email</a>
            </div>
        </div>
    </div>
</main>
