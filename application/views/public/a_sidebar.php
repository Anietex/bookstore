<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<main>
    <div class="container">
        <div class="wrapper p-2">
            <div class="row">
                <div class="col-sm-3">
                    <ul class="list-group">
                        <li class="list-group-item"><a href="<?=site_url('customer')?>">Edit Profile</a> </li>
                        <li class="list-group-item"><a href="<?=site_url('customer/online_read')?>">Online Readings</a> </li>
                        <li class="list-group-item"><a href="<?=site_url('customer/download_history')?>">Download History</a> </li>
                        <li class="list-group-item"><a href="<?=site_url('customer/order_history')?>">Order History</a> </li>
                    </ul>
                </div>
                <div class="col-sm-9">
