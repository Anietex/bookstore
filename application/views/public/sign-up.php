<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<main>
    <div class="form-wrapper sign-up my-5">
      <div class="row">
          <div class="col-sm-6 m-auto">
              <?=$msg->display()?>
              <div class="card">
                  <form action="<?=site_url('account/create')?>" method="post" id="sign-up-form" data-toggle="validator" role="form">
                  <h4 class="card-header">Sign up</h4>
                  <div class="card-body">
                          <div class="row">
                              <div class="form-group col-sm-6">
                                  <label for="first-name">First Name</label>
                                  <input type="text" class="form-control" id="first-name" name="first_name" value="<?=FlashData::get('first')?>" data-error="Enter first name" required>
                              </div>
                              <div class="form-group col-sm-6">
                                  <label>Last Name</label>
                                  <input type="text" class="form-control" name="last_name"  value="<?=FlashData::get('last')?>" required>
                              </div>
                              <div class="form-group col-12">
                                  <label>Gender</label><br>
                                  <div class="form-control">
                                      <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                              <input class="form-check-input" type="radio" name="gender" <?=FlashData::get('female')=='Male'?'checked':''?> value="Male" required> Male
                                          </label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                          <label class="form-check-label">
                                              <input class="form-check-input" type="radio" name="gender" <?=FlashData::get('female')=='Female'?'checked':''?> value="Female" required> Female
                                          </label>
                                      </div>
                                  </div>
                              </div>

                              <div class="form-group col-sm-6">
                                  <label>Phone Number</label>
                                  <input type="text" class="form-control" name="phone_no"  value="<?=FlashData::get('phone')?>" required>
                              </div>

                              <div class="form-group col-sm-6">
                                      <label>Email</label>
                                      <input type="email" class="form-control" name="email"  value="<?=FlashData::get('email')?>" required>
                                  </div>

                              <div class="form-group col-12" >
                                      <label>Address</label>
                                      <input type="text" class="form-control" name="address"  value="<?=FlashData::get('address')?>" required>
                                  </div>
                              <div class="form-group col-sm-6">
                                  <label>Password</label>
                                  <input type="password" class="form-control" name="password" id="password"  value="<?=FlashData::get('password')?>" required>
                              </div>
                              <div class="form-group col-sm-6">
                              <label>Confirm Password</label>
                              <input type="password" class="form-control" name="confirm_password" data-parsley-equalto="#password"  value="<?=FlashData::get('password1')?>" data-parsley-equalto-message="Password do not match" required>
                          </div>
                          </div>
                  </div>
                  <div class="card-footer">
                      <button type="submit" class="btn btn-primary" >Sign Up</button>
                      <p class="text-muted d-inline">Already Have an account <a href="<?=site_url('sign-in')?>">Sign In Here</a> </p>
                  </div>
                  </form>
              </div>
          </div>
      </div>
    </div>
</main>
