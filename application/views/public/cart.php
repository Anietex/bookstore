<main>
    <div class="container">
        <div class="">
            <h4>My Shopping Cart</h4>
        </div>
        <div class="my-5">
            <?php if($items):?>
            <?php foreach ($items as $item):?>
            <div class="row border p-1">
                <div class="col-3 col-sm-1 col-md-1">
                    <img src="<?=site_url('uploads/covers/'.$item['cover'])?>" class="img-fluid">
                </div>
                <div class="col-9 col-sm-10 col-md-10">
                    <form action="<?=site_url('cart/update')?>" method="post" class="cart-from">
                    <h6><?=$item['title']?></h6>
                    <div class="row">
                        <div class="col-sm-2 col-3">

                               <input type="hidden" name="id" value="<?=$item['id']?>">
                               <div class="form-group">
                                   <strong><p class="p-0 m-0">Copies</p> </strong>
                                   <select class="form-control-sm p-0 qty" name="qty" >
                                       <?php CusHelper::genQty($item['copies'],$item['qty'])?>
                                   </select>
                               </div>

                        </div>
                        <div class="col-sm-2 col-3">
                            <strong><p class="p-0 m-0">Price</p></strong>
                            &#x20a6;<?=number_format($item['price'])?>
                        </div>
                        <div class="col-sm-2  col-3">
                            <strong><p class="p-0 m-0">Subtotal</p></strong>
                            &#x20a6;<?=number_format($item['subtotal'])?>
                        </div>
                        <div class="col-sm-6 text-right col-3">
                            <div class="col-1 pt-2">
                                <button type="submit" name="remove" value="<?=$item['id']?>" class="btn btn-danger btn-sm" title="Remove"><span class="fa fa-close"></span></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
            <?php endforeach;?>
            <div class="row border py-3">
                <div class="col-sm-6 col-12 ">
                    <div class="row">
                        <div class="col-3 ">
                            <h5>Total Cost:</h5>
                        </div>
                        <div class="col-7">
                            <h5>&#x20a6;<?=number_format($total)?></h5>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <a href="<?=site_url('cart/checkout/summary')?>" class="btn btn-primary"><span class="fa fa-cart-arrow-down"></span>   Checkout</a>
                </div>
            </div>
            <?php else:?>
            <div class="text-center py-5">
                <h5 class="text-muted">You currently do not have any item in your cart</h5>
                <a href="<?=site_url('books')?>" class="btn btn-primary">Continue shopping</a>
            </div>
    <?php endif;?>
        </div>
    </div>
</main>