<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
    <div class="container read-online">

    <div class="reader">
        <div class="header bg-primary">
            <p class="book-title text-white"><?=$book_title?></p>
                    <hr class="m-0">
                <div class="actions bg-primary">
                    <div class="buttons btn-toolbar bg-primary">
                        <div class="btn-group">
                            <button class="btn btn-primary" id="prev-btn" title="Previous Page"><span class="fa fa-angle-double-left"></span> Previous</button>
                            <button class="btn btn-primary" id="next-btn" title="Next Page">Next <span class="fa fa-angle-double-right"></span></button>
                        <div class="pages btn-group">
                            <button class="btn btn-primary" ><span id="current"></span>/<span id="total"></span></button>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
        <div class="body">
            <div class="viewer-wrapper">
                <div class="overlay"></div>
                <div class="loading" id="loader"></div>
                <canvas id="viewer" width="900px" data-uri="<?=$uri?>">
                    Your browser does not support online reading of our e-books, try upgrading to a more modern browser

                </canvas>
            </div>
        </div>
    </div>
    </div>
</main>
<script type="text/javascript" src="<?=site_url('assets/js/pdfjs/pdf.js')?>"></script>
<script type="text/javascript" src="<?=site_url('assets/js/pdfjs/pdf.js')?>"></script>
<script type="text/javascript" src="<?=site_url('assets/js/pdf-viewer.js')?>"></script>
<script type="text/javascript">



</script>