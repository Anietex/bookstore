<main>
    <div class="wrapper py-5 container">
        <div class="inner-404 text-center py-5">
            <h1>404</h1>
            <p>The resource you are looking for does not exist</p>
        </div>
    </div>
</main>