<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
    <div class="container">
        <div class="reg-success py-5 mb-3">
            <h2>Unactivated Account </h2>
            <?=$msg->display();?>
                <p>Your account has not yet been activated please check your email(<strong><?=$email?></strong>) inbox in order to activate your account</p>
                <p>You did not receive any email?, check your spam folder or click the button below to resend the email</p>
            <a href="<?=site_url('account/resend?to='.$email)?>" class="btn btn-primary">Resend Email</a>

        </div>
    </div>
</main>
