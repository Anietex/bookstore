<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
                   <section class="profile" >
                        <h3>Edit Profile</h3>
                       <?=$msg->display()?>
                        <div class="border p-3 rounded">
                            <form action="<?=site_url('account/update')?>" method="post">
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <label>First Name</label>
                                        <input type="text" class="form-control" name="first_name" value="<?=FlashData::get('first',$customer->first_name)?>" required>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control" name="last_name"  value="<?=FlashData::get('last',$customer->last_name)?>" required>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>Gender</label><br>
                                        <div class="form-control">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="radio" name="gender" <?=FlashData::get('female',$customer->gender)=='Male'?'checked':''?> value="Male" required> Male
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="radio" name="gender" <?=FlashData::get('female',$customer->gender)=='Female'?'checked':''?> value="Female" required> Female
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label>Phone Number</label>
                                        <input type="text" class="form-control" name="phone_no"  value="<?=FlashData::get('phone',$customer->phone_no)?>" required>
                                    </div>

                                    <div class="form-group col-sm-6">
                                        <label>Email</label>
                                        <input type="email" class="form-control" name="email"  value="<?=FlashData::get('email',$customer->email)?>" required>
                                    </div>

                                    <div class="form-group col-12" >
                                        <label>Address</label>
                                        <input type="text" class="form-control" name="address"  value="<?=FlashData::get('address',$customer->address)?>" required>
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>New Password</label>
                                        <input type="password" class="form-control" name="password">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label>Old Password</label>
                                        <input type="password" class="form-control" name="old-password"  value="<?=FlashData::get('password1')?>" required>
                                        <small class="text-muted">Enter your old password to update your changes</small>
                                    </div>
                                </div>
                                <hr>
                                <button type="submit" class="btn btn-warning">Update</button>
                            </form>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
