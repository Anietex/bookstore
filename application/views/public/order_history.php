<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="profile" >
    <h4>Order History</h4>
    <div class="border p-1 rounded">
        <?php if($orders):?>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>S/N</th>
                    <th>Tracking Code</th>
                    <th>Transaction ID</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
            <?php $i =1; foreach ($orders as $order):?>
                <tr>
                    <td><?=$i++?></td>
                    <td>TK<?=str_pad($order->order_id,5,'0',STR_PAD_LEFT)?></td>
                    <td><?=$order->transaction_ref?></td>
                    <td><?=$order->order_date?></td>
                    <td><?=$order->confirmed==1?'Confirmed':'Unconfirmed'?></td>
                    <td><a href="<?=site_url('customer/order_details/'.$order->order_id)?>">View Details</a> </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php else:?>
            <div class="text-center py-3">
                <h5 class="text-muted">You currents do not have any order History</h5>
            </div>
        <?php endif;?>
    </div>
</section>
</div>
</div>
</div>
</div>
</main>
