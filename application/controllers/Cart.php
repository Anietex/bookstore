<?php
use Yabacon\Paystack;
use Yabacon\Paystack\Exception\PaystackException;

class Cart extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model(['BookModel','CustomerModel','OrderedItemModel','OrdersModel','CategoryModel']);
        $this->load->library(['session','FlashMessages','FlashData','Auth','cart','CartManager','CusHelper','CodeGenerator']);
        $this->cart = new CartManager();
        $this->auth = new Auth();
        $this->msg = new FlashMessages();

    }

    public function index(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Your shopping Cart";
        $data['items'] = $this->cart->getItems();
        $data['total'] = $this->cart->getTotalCost();
        $this->load->view("public/header",$data);
        $this->load->view('public/cart',$data);
        $this->load->view('public/footer');
    }

    public function checkout($prog='summary'){
        if(Auth::isLoggedIn()){
            if(!$this->auth->accountIsActivated()){
                redirect(site_url('unactivated'));
            }

            $options = ['summary','shipping','payment','complete'];
            $customer = $this->CustomerModel->getCustomer(['customer_id'=>$_SESSION['customer_id']]);
            if(in_array($prog,$options)){
                $data['items'] = $this->cart->getItems();
                $data['total'] = $this->cart->getTotalCost();
                $data['prog'] =$prog;
                $data['customer'] = $customer;
                $data['msg'] = $this->msg;

                switch ($prog){
                    case "summary":
                        $data['title'] = "Checkout | Summary";
                        break;
                    case 'shipping':
                        $data['title'] = "Checkout | Shipment";
                        if(empty($_SESSION['customer_name'])||empty( $_SESSION['email']) || empty($_SESSION['address']) ){
                            $_SESSION['customer_name'] = $customer->first_name.' '.$customer->last_name;
                            $_SESSION['email']=$customer->email;
                            $_SESSION['address'] =$customer->address;
                            $_SESSION['phone'] =$customer->phone_no;
                        }
                        break;
                    case 'complete':
                        $data['title'] = "Order completed";
                        $data['error'] = false;
                        if(!empty($_GET['ref']) && is_numeric($_GET['ref'])){
                            $order = $this->OrdersModel->getOrder(['order_id'=>$_GET['ref']]);
                            if(!$order){
                                $this->orderFailure();
                                exit();
                            }else{
                              $data['trans_ref'] = $order->transaction_ref;
                              $data['order_id'] = 'TK'.str_pad($_GET['ref'],5,'0',0);
                            }
                        }
                        break;
                    case "payment":
                        $data['title'] = "Checkout | Payment";
                }
                $data['categories'] = $this->CategoryModel->getCategories();
                $this->load->view("public/header",$data);
                $this->load->view('public/checkout',$data);
                $this->load->view('public/footer');
            }else{
                redirect(site_url('cart'));
            }
        }else{
            $this->msg->info("Please log in to checkout items in your shopping cart");
            $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            redirect(site_url('sign-in?ref='.$link));
        }
    }

    public function addItem($id){
        $item['id'] = $id;
        $item['qty'] =1;
        $this->cart->addItem($id,$item);
        redirect($_SERVER['HTTP_REFERER']);
    }


    public function update(){
        if(isset($_POST['remove'])){
            $this->cart->remove($_POST['id']);
        }else{
            $this->cart->updateQty($_POST['id'],$_POST['qty']);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function update_shipping_info(){
        $_SESSION['customer_name'] = $_POST['customer_name'];
        $_SESSION['email']=$_POST['email'];
        $_SESSION['address'] =$_POST['address'];
        $_SESSION['phone'] =$_POST['phone'];
        redirect($_SERVER['HTTP_REFERER']);

    }




































    public function read_checkout($id=''){
        if(Auth::isLoggedIn()){

        $data['customer'] = $this->CustomerModel->getCustomer(['customer_id'=>$_SESSION['customer_id']]);
        $data['book'] = $this->BookModel->getBook($id);

        if($data['book'] ==null){
            $this->not_found();
        }else{
            $this->load->view("public/header");
            $this->load->view('public/read_checkout',$data);
            $this->load->view('public/footer');
        }
        }else{
            $this->msg->info("Please log in to read online");
            redirect(site_url('sign-in'));
        }
    }




    public function not_found(){
        $this->load->view("public/header");
        $this->load->view('public/not_found');
        $this->load->view('public/footer');
    }

    private function orderFailure(){
        $this->load->view("public/header");
        $this->load->view('public/not_found');
        $this->load->view('public/footer');
    }

}