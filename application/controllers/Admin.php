<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['url_helper']);
        $this->load->library(['session','FlashMessages','FlashData','DashboardHelper','StatisticsHandler']);
        $this->load->model(['CategoryModel','BookModel','ReadModel','OrdersModel','OrderedItemModel','CustomerModel']);
        $this->msg = new FlashMessages();


        if(!isset($_SESSION['username'])){
            redirect(site_url('admin/sign_in'));
        }
    }


    public function dashboard(){
        $this->index();
    }


    public function index(){
       $stat = new StatisticsHandler();
        $orderStat = $stat->getOrdersStatistics();
        $data['hardRev'] = $stat->getTotalHardCopySales();
        $data['ebookRev'] = $stat->getTotalEbookSales();
        $data['readRev'] = $stat->getTotalReadingsSales();
        $data['totalCustomers'] = $stat->getTotalCustomers();
        $data['confirmed'] = $orderStat['confirmed'];
        $data['completed'] = $orderStat['completed'];
        $data['unconfirmed']=$orderStat['unconfirmed'];
        $data['title'] = "Dashboard";

        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/dashboard",$data);
        $this->load->view("admin/footer");
    }

    public function book(){
        $data['msg']=$this->msg;
        $data['title'] = "Add Book";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/book",$data);
        $this->load->view("admin/footer");
    }

    public function manage_books(){
        $data['books'] = $this->BookModel->getBooksAdmin();
        $data['msg']=$this->msg;
        $data['title'] = "Manage Books";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/manage_books",$data);
        $this->load->view("admin/footer");
    }


    public function book_details($id='-1'){
        $book = $this->BookModel->getBook($id);
        $data['book'] = $book;
        $data['msg']=$this->msg;
        $data['title'] = "Book details | ".$book->title;
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/book_details",$data);
        $this->load->view("admin/footer");
    }

    public function categories(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['msg']=$this->msg;
        $data['title'] = "Manage Category";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/categories",$data);
        $this->load->view("admin/footer");
    }

    public function customers(){
        $data['customers'] = $this->CustomerModel->getCustomers();
        $data['msg']=$this->msg;
        $data['title'] = "Manage Customers";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/customers",$data);
        $this->load->view("admin/footer");
    }

    public function orders(){
        $data['orders'] = $this->OrdersModel->getOrders();
        $data['msg']=$this->msg;
        $data['title'] = "All Orders";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/orders",$data);
        $this->load->view("admin/footer");
    }

    public function confirmed_orders(){
        $data['orders'] = $this->OrdersModel->getConfirmedOrders();
        $data['msg']=$this->msg;
        $data['title'] = "Confirm Orders";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/confirmed_orders",$data);
        $this->load->view("admin/footer");
    }

    public function unconfirmed_orders(){
        $data['orders'] = $this->OrdersModel->getUnconfirmedOrders();
        $data['msg']=$this->msg;
        $data['title'] = "Unconfirmed Orders";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/unconfirmed_orders",$data);
        $this->load->view("admin/footer");
    }

    public function completed_orders(){
        $data['orders'] = $this->OrdersModel->getCompletedOrders();
        $data['msg']=$this->msg;
        $data['title'] = "Completed Orders";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/completed_orders",$data);
        $this->load->view("admin/footer");
    }


    public function order_details($order_id)
    {
        $data['order'] = $this->OrdersModel->getOrder($order_id);
        $data['books'] = $this->OrderedItemModel->getOrderedItems($order_id);

        $data['msg'] = $this->msg;
        $data['title'] = "Order Details | TK".str_pad($order_id,5,'0');
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/order_details", $data);
        $this->load->view("admin/footer");
    }


    public function order_history($customer_id){
        $this->load->model(['EbookModel','ReadModel']);
        $customer = $this->CustomerModel->getCustomer(['customer_id'=>$customer_id]);
        $data['customer_name'] = $customer->first_name.' '.$customer->last_name;
        $data['orders'] = $this->OrdersModel->getCustomerOrders($customer_id);
        $data['ebooks'] = $this->EbookModel->getCustomerEbooks($customer_id);
        $data['readings'] = $this->ReadModel->getCustomerBooks($customer_id);
        $data['title'] = "Customer order history";
        $data['msg']=$this->msg;
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/order_history",$data);
        $this->load->view("admin/footer");
    }



    public function ebook_sales(){
        $this->load->model(['EbookModel']);
        $data['books'] = $this->EbookModel->getSales();
        $data['msg']=$this->msg;
        $data['title'] = "Ebook sales";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/ebook_sales",$data);
        $this->load->view("admin/footer");
    }

    public function online_reading(){
        $this->load->model(['ReadModel']);
        $data['books'] = $this->ReadModel->getSales();
        $data['msg']=$this->msg;
        $data['title']= "Online Readings";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/online_reading",$data);
        $this->load->view("admin/footer");
    }

    public function edit_account(){
        $data['msg']=$this->msg;
        $data['title'] = "Edit account";
        $this->load->view("admin/header",$data);
        $this->load->view("admin/sidebar");
        $this->load->view("admin/edit_account",$data);
        $this->load->view("admin/footer");
    }

    public function sign_out(){

        unset($_SESSION['username']);
        redirect(site_url('admin/sign_in'));
    }




}