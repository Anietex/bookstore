<?php

class Ebook extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model(['BookModel','CustomerModel','EbookModel','CategoryModel']);
        $this->load->library(['session','FlashMessages','FlashData','Auth','cart','CartManager','CusHelper','CodeGenerator']);
        $this->cart = new CartManager();
        $this->auth = new Auth();
        $this->msg = new FlashMessages();
    }



    public function buy($id,$prog='billing'){
        if(Auth::isLoggedIn()) {

            $options = ['billing', 'payment', 'download'];
            $book = $this->BookModel->getBook($id);

            if (in_array($prog, $options)){

                $data['customer'] = $this->CustomerModel->getCustomer(['customer_id' => $_SESSION['customer_id']]);
                $data['book'] = $this->BookModel->getBook($id);
                $data['prog'] = $prog;
                $data['msg'] = $this->msg;
                $data['categories'] = $this->CategoryModel->getCategories();

                if ($data['book'] == null) {
                    $this->not_found();
                } else {
                    switch ($prog){
                        case "billing":
                            $data['title'] = "Ebook Download | Billing Info";
                            if(CusHelper::alreadyDownloaded($id,$_SESSION['customer_id'])){
                                $ebook = $this->EbookModel->getEbook(['book_id'=>$id]);
                                if($ebook->customer_id=$_SESSION['customer_id']){
                                    redirect(site_url('ebook/download/'.$ebook->download_id));
                                    exit();
                                }
                            }
                            break;
                        case "download":
                            $data['title'] = "Ebook Download | Download";
                            if(!empty($_GET['udid'])){
                                $data['udid'] = $_GET['udid'];
                            }else{
                                $this->msg->error("Something went wrong your request could not be process");
                                redirect(site_url('ebook/buy/'.$book->book_id.'/payment'));
                            }
                            break;
                        case "payment":
                            $data['title'] = "Ebook Download | Payment";
                    }

                    $this->load->view("public/header",$data);
                    $this->load->view('public/ebook_checkout', $data);
                    $this->load->view('public/footer');
                }
            }
            else {
                redirect(site_url());
            }
        }else{
            $this->msg->info("Please log in to download");
            $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            redirect(site_url('sign-in?ref='.$link));
        }
    }

    public function download($udid=''){
        if(Auth::isLoggedIn()){
            $ebook = $this->EbookModel->getEbook(['download_id'=>$udid]);

            if($ebook->customer_id!=$_SESSION['customer_id']){
                $this->not_found();
                exit();
            }
            if($ebook){
                $book = $this->BookModel->getBook($ebook->book_id);
                $file = 'uploads/ebooks/'.$book->ebook_file;
                $this->EbookModel->increaseDownload($udid);
                CusHelper::forceDownload($file,$book->title.'.pdf');
            }else{
                $this->not_found();
            }

        }else{
            $this->msg->info("Please log in to download");
            $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            redirect(site_url('sign-in?ref='.$link));
        }
    }

    public function authPayment($ref=''){
        $paystack = new \Yabacon\Paystack('sk_test_e9c90309309a79ed3ed11ff852a4c74779080dca');
        print "<pre>";
        try{
            $responseObj = $paystack->transaction->verify(["reference"=>$ref]);
            $message = $responseObj->message;
            $reference = $responseObj->data->reference;
            $status = $responseObj->data->status;
            $customer_id = $responseObj->data->metadata->custom_fields->customer->customer_id;
            $book_id = $responseObj->data->metadata->custom_fields->book->book_id;
            $gw_status = $responseObj->data->gateway_response;
            $udid = sha1(mt_rand());
            $amount = $responseObj->data->amount/100;

            if(($message == "Verification successful" || $message = "madePayment") && $status == "success" && ($gw_status == 'Successful' || $gw_status == "Approved")){
                    if($this->EbookModel->create($customer_id,$book_id,$reference,$amount,$udid)){
                        redirect(site_url('ebook/buy/'.$book_id.'/download/?udid='.$udid));
                    }else{
                        $this->msg->error('Something went wrong');
                        redirect(site_url('ebook/buy/'.$book_id.'/payment'));
                    }
            }
        }catch (Yabacon\Paystack\Exception\ApiException $e){
            print_r($e->getResponseObject());
        }
    }



    public function not_found(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "404 Not found";
        $this->load->view("public/header",$data);
        $this->load->view('public/not_found');
        $this->load->view('public/footer');
    }


}