<?php


class Read extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model(['BookModel','CustomerModel','ReadModel','CategoryModel']);
        $this->load->library(['session','FlashMessages','FlashData','Auth','cart','CartManager','CusHelper','CodeGenerator']);
        $this->cart = new CartManager();
        $this->auth = new Auth();
        $this->msg = new FlashMessages();
    }

    public  function buy($book_id,$prog = "billing"){
        if(Auth::isLoggedIn()) {
            $options = ['billing', 'payment', 'read'];
            $book = $this->BookModel->getBook($book_id);

            if (in_array($prog, $options)){

                $data['customer'] = $this->CustomerModel->getCustomer(['customer_id' => $_SESSION['customer_id']]);
                $data['book'] = $book;
                $data['prog'] = $prog;
                $data['msg'] = $this->msg;
                $data['categories'] = $this->CategoryModel->getCategories();

                if ($data['book'] == null) {
                    $this->not_found();
                } else {
                    switch ($prog){
                        case "billing":
                            $data['title'] = "Read | Billing ";
                            break;
                        case 'payment':
                            $data['title'] = "Read | Payment ";
                            break;
                        case "read":
                            if(!empty($_GET['urid'])){
                                $data['urid'] = $_GET['urid'];
                            }else{
                                $this->msg->error("Something went wrong your request could not be process");
                                redirect(site_url('read/buy/'.$book->book_id.'/payment'));
                            }

                    }

                    $this->load->view("public/header",$data);
                    $this->load->view('public/read_checkout', $data);
                    $this->load->view('public/footer');
                }
            }
            else {
                redirect(site_url());
            }
        }else{
            $this->msg->info("Please log in to download");
            $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            redirect(site_url('sign-in?ref='.$link));
        }
    }

    public function authPayment($ref=''){
        $paystack = new \Yabacon\Paystack('sk_test_e9c90309309a79ed3ed11ff852a4c74779080dca');
        print "<pre>";
        try{
            $responseObj = $paystack->transaction->verify(["reference"=>$ref]);
            $message = $responseObj->message;
            $reference = $responseObj->data->reference;
            $amount = $responseObj->data->amount/100;
            $status = $responseObj->data->status;
            $customer_id = $responseObj->data->metadata->custom_fields->customer->customer_id;
            $book_id = $responseObj->data->metadata->custom_fields->book->book_id;
            $gw_status = $responseObj->data->gateway_response;
            $urid = sha1(mt_rand());

            if(($message == "Verification successful" || $message = "madePayment") && $status == "success" && ($gw_status == 'Successful' || $gw_status == "Approved")){
                if($this->ReadModel->create($customer_id,$book_id,$reference,$amount,$urid)){
                    redirect(site_url('read/buy/'.$book_id.'/read/?urid='.$urid));
                }else{
                    $this->msg->error('Something went wrong');
                    redirect(site_url('read/buy/'.$book_id.'/payment'));
                }
            }
           // print_r(  $responseObj);


        }catch (Yabacon\Paystack\Exception\ApiException $e){
            print_r($e->getResponseObject());
        }
    }

    public function not_found(){
        $this->load->view("public/header");
        $this->load->view('public/not_found');
        $this->load->view('public/footer');
    }
}