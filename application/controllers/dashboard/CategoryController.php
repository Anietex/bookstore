
<?php

class CategoryController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['url_helper']);
        $this->load->library(['session','FlashMessages','FlashData']);
        $this->load->model('CategoryModel');
        $this->msg = new FlashMessages();
    }

    public function create(){
        if($this->CategoryModel->categoryExist($_POST['cat_name'])){
            $this->msg->error("Category already exist");
        }else{
            if($this->CategoryModel->create())
                $this->msg->success("Category was successfully added");
            else
                $this->msg->error("Category was not added due to system error");
        }
        redirect(site_url('admin/categories'));
    }


    public function update(){
       if($this->CategoryModel->update())
           $this->msg->success("category was updated successfully");
       else
           $this->msg->error("category information was not updated due to system error");
       redirect(site_url('admin/categories'));
    }

    public function delete($cat_id){
        if($this->CategoryModel->delete($cat_id))
            $this->msg->success('Category was deleted successfully');
        else
            $this->msg->error('Category was successfully deleted');
        redirect(site_url('admin/categories'));
    }


    public function edit($cat_id){
        $cat = $this->CategoryModel->getCategory($cat_id);
       FlashData::set('cat_name',$cat->cat_name);
       FlashData::set('cat_id',$cat->cat_id);
       FlashData::set('btn-text',"Update");
       FlashData::set('editing',true);
       FlashData::set('action','update');
       redirect($_SERVER['HTTP_REFERER']);
    }


}