<?php

class OrdersController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(['session','FlashMessages','FlashData']);
        $this->load->library('EmailSender');
        $this->load->model(['BookModel','OrdersModel']);
        $this->load->helper(['url_helper']);
        $this->msg = new FlashMessages();
    }


    public function confirm($order_id){
        $order = $this->OrdersModel->getOrder($order_id);
        if($order){
            $this->OrdersModel->updateConfirm($order_id);
            $this->sendConfirmationEmail($order);
            $this->msg->success("Order was confirm successfully");
        }
        redirect($_SERVER['HTTP_REFERER']);
    }


    public function complete($order_id){
        $order = $this->OrdersModel->getOrder($order_id);
        if($order){
            $this->OrdersModel->updateComplete($order_id);
            $this->msg->success("Order was  successfully mark as completed");
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    private  function sendConfirmationEmail($order){
        $to = $order->email;
        $customer_name = $order->first_name.' '.$order->last_name;
        $tk = 'TK'.str_pad($order->order_id,5,'0',0);
        $msg = "Dear ".$customer_name.", your order ".$tk." has been confirmed and has been shipped";
        $subj = "BookStore | ".$tk." Order Confirmation";
        EmailSender::send($to,$subj,$msg);
    }

}