<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BookController extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(['session','FlashMessages','FlashData']);
        $this->load->library('upload');
        $this->load->model(['BookModel']);
        $this->load->helper(['url_helper']);
        $this->msg = new FlashMessages();
    }

    public function create(){
        $ebook = '';
        $cover = '';
        $coverError = false;
        $ebookError = false;
        $ebookErrMsg = '';
        $coverErrMsg = '';

        if(!empty($_FILES['ebook'])){
            $ebook_upload = $this->uploadEbook();
            if($ebook_upload['status'] == 'success'){
                $ebook = $ebook_upload['file_name'];
            }else{
                $ebookError = true;
                $ebookErrMsg =$ebook_upload['errors'];
            }

        }

        $cover_upload = $this->uploadCover();
        if($cover_upload['status'] == 'success'){
            $cover = $cover_upload['file_name'];
        }else{
            $coverError  = true;
            $coverErrMsg = $cover_upload['errors'];
        }

        if($this->BookModel->create($ebook,$cover)){
            if($coverError || $ebookError){

                $this->msg->error('Book was successfully added but there was the following upload error(s) '.$ebookErrMsg.$coverErrMsg);
            }else{
                $this->msg->success("Book was added successfully");
            }
        }
        redirect($_SERVER['HTTP_REFERER']);

    }

    public function update($book_id){
        $coverError = false;
        $ebookError = false;
        $ebookErrMsg = '';
        $coverErrMsg = '';
        $book = $this->BookModel->getBook($book_id);
        $ebook = $book->ebook_file;
        $cover = $book->cover_photo_file;


        if($_FILES['ebook']['size']!=0){
            $this->deleteEbook($book->ebook_file);
            $ebook_upload = $this->uploadEbook();
            if($ebook_upload['status'] == 'success'){
                $ebook = $ebook_upload['file_name'];
            }else{
                $ebookError = true;
                $ebookErrMsg =$ebook_upload['errors'];
            }
        }


        if($_FILES['book_cover']['size']!=0){
            $this->deleteCover($book->cover_photo_file);
            $cover_upload = $this->uploadCover();
            if($cover_upload['status'] == 'success'){
                $cover = $cover_upload['file_name'];
            }else{
                $coverError  = true;
                $coverErrMsg = $cover_upload['errors'];
            }

        }

        if($this->BookModel->update($book_id,$ebook,$cover)){
            if($coverError || $ebookError){

                $this->msg->error('Book was successfully Updated but there was the following upload error(s) '.$ebookErrMsg.$coverErrMsg);
            }else{
                $this->msg->success("Book was update successfully");
            }
        }

        redirect(site_url('book/edit/'.$book_id));
    }



    public function edit($book_id){
        $book = $this->BookModel->getBook($book_id);
        FlashData::set('title',$book->title);
        FlashData::set('category',$book->cat_name);
        FlashData::set('author',$book->author);
        FlashData::set('year',$book->year_published);
        FlashData::set('pages',$book->pages);
        FlashData::set('copies',$book->copies);
        FlashData::set('hardcopy',$book->hardcopy_price);
        FlashData::set('ebook',$book->ebook_price);
        FlashData::set('read',$book->read_price);
        FlashData::set('edition',$book->edition);
        FlashData::set('isbn',$book->isbn);
        FlashData::set('description',$book->description);
        FlashData::set('cover','../uploads/covers/'.$book->cover_photo_file);
        FlashData::set('show_cover',true);
        FlashData::set('show_btn',true);
        FlashData::set('btnText','Update');
        FlashData::set('action_page','update/'.$book_id);
        FlashData::set('page_title','Update book details');
        FlashData::set('cover_required','r');
        FlashData::set('ebook_required','r');
        redirect(site_url('admin/book'));

    }

    private function uploadEbook(){
        $config['upload_path'] = './uploads/ebooks';
        $config['allowed_types'] = 'pdf';
        $config['file_name'] = $_POST['book_title'];
        $this->upload->initialize($config,true);
        if($this->upload->do_upload('ebook')){
            return ['status' =>"success",'file_name'=>$this->upload->data('file_name')];
        }else{
            return ['status' => "failure" ,'errors'=>$this->upload->display_errors('<p> e-book: ','</p>')];
        }
    }
    private function uploadCover(){
        $config['upload_path'] = './uploads/covers';
        $config['allowed_types'] = 'jpg|png|gif|jpeg';
        $config['file_name'] = $_POST['book_title'];
        $config['file_ext_tolower'] = true;
        $this->upload->initialize($config,true);

        if($this->upload->do_upload('book_cover')){
            return ['status' =>"success",'file_name'=>$this->upload->data('file_name')];
        }else{
            return ['status' => "failure" ,'errors'=>$this->upload->display_errors('<p> Book cover: ','</p>')];
        }

    }

    public  function delete($book_id){
        $book = $this->BookModel->getBook($book_id);
        $delOption = $_POST['delete-option'];
        if($delOption == 'only_book'){
            if($this->BookModel->softDelete($book_id))
                $this->msg->success("Book was deleted successfully");
            else
                $this->msg->error("Book was not deleted due to some error");
        }else{

           if(!empty($book->ebook_file))
                $this->deleteEbook($book->ebook_file);
          if(!empty($book->cover_photo_file))
                $this->deleteCover($book->cover_photo_file);

            if($this->BookModel->delete($book_id))
                $this->msg->success("Book was deleted successfully");
            else
                $this->msg->error("Book was not deleted due to some error");

        }

       redirect($_SERVER['HTTP_REFERER']);
    }

    private function deleteEbook($ebook){
        $file = 'uploads/ebooks/'.$ebook;
        if(file_exists($file)){
           return unlink($file);
        }
        
        return false;
    }
    private function deleteCover($cover){
        $file = 'uploads/covers/'.$cover;
        print $file;
        if(file_exists($file)){
            return unlink($file);
        }

        return false;
    }
}