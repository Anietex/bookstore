<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public $msg;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['url_helper']);
        $this->load->model(['BookModel','CustomerModel','CategoryModel']);
        $this->load->library(['session','FlashMessages','FlashData','Auth','CartManager','TokenManager','CusHelper']);
        $this->auth = new Auth();
        $this->msg = new FlashMessages();
        $this->tokenMgr = new TokenManager();



    }

    public function index()
	{
//        if($this->auth->isLoggedIn()){
//            if(!$this->auth->accountIsActivated()){
//                redirect(site_url('unactivated'));
//                exit();
//            }
//        }
        $data['allBooks'] = $this->BookModel->getBooks(0,12);
        $data['popularBooks'] = $this->BookModel->getPopularBooks();
        $data['newBooks'] = $this->BookModel->getNewBooks();
        $data['randomBooks'] = $this->BookModel->getRandomBooks();
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Welcome, Buy Books, Buy Ebooks, Read Books Online";
	    $this->load->view("public/header",$data);
		$this->load->view('public/index',$data);
        $this->load->view('public/footer');
	}

	public function book($book_id=''){
        $data['book'] = $this->BookModel->getBook($book_id);
        $data['title'] = $data['book']->title.' book details';
        $data['categories'] = $this->CategoryModel->getCategories();
        if($data['book']){
            $this->load->view("public/header",$data);
            $this->load->view('public/book',$data);
            $this->load->view('public/footer');
        }else{
            redirect(site_url('books'));
        }
    }


    public function sign_up(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Sign Up";
        $this->load->view("public/header",$data);
        if(FlashData::get('reg-success',false) == true)
            $this->load->view('public/reg_success',['msg' =>$this->msg]);
        else
            $this->load->view('public/sign-up',['msg' =>$this->msg]);
         $this->load->view('public/footer');
    }

    public function sign_in(){

        if($this->tokenMgr->tokenExist()){
            if($this->tokenMgr->AuthToken()){
                if(isset($_GET['ref']))
                    redirect($_GET['ref']);
                else
                    redirect(site_url());
            }
        }
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Sign In";
        $this->load->view("public/header",$data);
        $this->load->view('public/sign-in',['msg' =>$this->msg]);
        $this->load->view('public/footer');
    }

    public function forget_password(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Forget Password";
        $this->load->view("public/header",$data);
        $this->load->view('public/forget_password',['msg' =>$this->msg]);
        $this->load->view('public/footer');
    }

    public function reset_password($hash='-1'){
        $this->load->model('PasswordResetModel');
        $reset = $this->PasswordResetModel->getResetData($hash);
        if($reset){
            $data['id'] = $reset->hash;
            $data['categories'] = $this->CategoryModel->getCategories();
            $data['title'] = "Reset Password";
            $this->load->view("public/header",$data);
            $this->load->view('public/new_password',$data);
            $this->load->view('public/footer');
        }else{
            redirect(site_url('forget_password'));
        }
    }

    public function activated(){
        if(FlashData::get('activation-ok',false)==true ){
            $data['categories'] = $this->CategoryModel->getCategories();
            $this->load->view("public/header",$data);
            $this->load->view('public/activated');
            $this->load->view('public/footer');
        }else{
            $this->msg->error("Something went wrong during your account activation");
            redirect(site_url('sign-up'));
        }
    }

    public function unactivated(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Unactivated Account";
        $data['email'] = $this->CustomerModel->getCustomer(['customer_id'=>$_SESSION['customer_id']])->email;
        $data['msg'] = $this->msg;
        $this->load->view("public/header",$data);
        $this->load->view('public/unactivated');
        $this->load->view('public/footer');
    }


    public function category_book($cat_id=0){
        $this->load->model('CategoryModel');
        $totalContent = count($this->BookModel->getBooksInCategory($cat_id));

        $config['total_content']=$totalContent;
        $config['content_per_page']=18;
        $config['no_of_links'] = 6;
        $this->load->library('Pager',$config);
        $pager = new Pager($config);
        $pager->setLink(site_url('books?page'));
        if(isset($_GET['page'])&& is_numeric($_GET['page'])){
            $data['books'] = $this->BookModel->getBooksInCategory($cat_id,$_GET['page']*18,18);
        }else{
            $data['books'] = $this->BookModel->getBooksInCategory($cat_id,0,18);
        }


        $data['categories'] = $this->CategoryModel->getCategories();
        $data['heading'] = $this->CategoryModel->getCategory($cat_id)->cat_name;
        $data['title'] = 'Category | '.$data['heading'];
        $data['pages'] = $pager->getLinks();
        $this->load->view("public/header",$data);
        $this->load->view('public/books',$data);
        $this->load->view('public/footer');

    }



    public function search(){
        if(empty($_GET['query'])){
            redirect(site_url('books'));
        }else{

            $totalContent = count($this->BookModel->getSearchedBook($_GET['query']));
            $config['total_content']=$totalContent;
            $config['content_per_page']=18;
            $config['no_of_links'] = 6;
            $this->load->library('Pager',$config);


            $pager = new Pager($config);
            $pager->setLink(site_url('books?query='.htmlentities($_GET['query']).'&page'));
            if(isset($_GET['page'])&& is_numeric($_GET['page'])){
                $data['books'] = $this->BookModel->getSearchedBook($_GET['query'],$_GET['page']*18,18);
            }else{
                $data['books'] = $this->BookModel->getSearchedBook($_GET['query'],0,18);
            }


            $data['categories'] = $this->CategoryModel->getCategories();
            $data['title'] = "Search | ".$_GET['query'];
            $data['pages'] = $pager->getLinks();
            $this->load->view("public/header",$data);
            $this->load->view('public/search',$data);
            $this->load->view('public/footer');
        }
    }





    public function books($page =0){
        $totalContent = count($this->BookModel->getBooks());

        $config['total_content']=$totalContent;
        $config['content_per_page']=18;
        $config['no_of_links'] = 6;
        $this->load->library('Pager',$config);


        $pager = new Pager($config);
        $pager->setLink(site_url('books?page'));
        if(isset($_GET['page'])&& is_numeric($_GET['page'])){
                $data['books'] = $this->BookModel->getBooks($_GET['page']*18,18);
        }else{
            $data['books'] = $this->BookModel->getBooks(0,18);
        }


        $data['categories'] = $this->CategoryModel->getCategories();
        $data['heading'] = "All Books";
        $data['title'] = "All Books";
        $data['pages'] = $pager->getLinks();
        $this->load->view("public/header",$data);
        $this->load->view('public/books',$data);
        $this->load->view('public/footer');
    }




    public function seeder(){
        $faker = Faker\Factory::create();
        for($i =0; $i<150; $i++){
            $title = $faker->realText(35);
            $cat=6;
            $author = $faker->name;
            $year = $faker->year;
            $pages = $faker->randomNumber(3);
            $copies=rand(10,35);
            $hard=rand(1500,3500);
            $ebook= $cover=$faker->file('assets/books','uploads/ebooks',false);
            $read=rand(250,1000);
            $des=$faker->realText(5000);
            $sales=rand(5,25);
            $cover=$faker->file('assets/images/covers','uploads/covers',false);
            $date = $faker->date();

           $this->BookModel->seed($title,$cat,$author,$year,$pages,$copies,$hard,$ebook,$read,$des,$sales,$cover,$date);
        }
        echo 'Done';



    }





}
