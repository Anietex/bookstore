<?php

class Checkout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model(['OrderedItemModel','OrdersModel','BookModel']);
        $this->load->library(['session','FlashMessages','FlashData','Auth','cart','CartManager','CusHelper']);
        $this->cart = new CartManager();
        $this->auth = new Auth();
        $this->msg = new FlashMessages();

    }


    public function authPayment($ref='-1'){
        // T591561583065675
        $paystack = new \Yabacon\Paystack('sk_test_e9c90309309a79ed3ed11ff852a4c74779080dca');
        try {
          // $responseObj = $paystack->transaction->verify(["reference" => 'T829605432098718']);

           $responseObj = $paystack->transaction->verify(["reference"=>$ref]);
            $message = $responseObj->message;
            $reference = $responseObj->data->reference;
            $amount = $responseObj->data->amount/100;
            $status = $responseObj->data->status;
            $customer = $responseObj->data->metadata->custom_fields->customer;
            $orderedItems = $responseObj->data->metadata->custom_fields->cart;
            $gw_status = $responseObj->data->gateway_response;

            if(($message == "Verification successful" || $message = "madePayment") && $status == "success" && ($gw_status == 'Successful' || $gw_status == "Approved")){
                $order_id = $this->createOrder($reference,$customer,$orderedItems,$amount);
                CartManager::clearCart();
               redirect(site_url('cart/checkout/complete?ref='.$order_id));
            } else {
                $this->msg->error('Something went wrong with your payment authentication');
                redirect(site_url('cart/checkout/payment'));
            }
//            print "<pre>";
//              print_r($responseObj);
        }catch (Yabacon\Paystack\Exception\ApiException $e){
            print_r($e->getResponseObject());
        }

    }

    private function createOrder($trans_ref,$customer,$items,$amount){
        $order_id = $this->OrdersModel->create($trans_ref,$customer->customer_id,$customer->phone_no,$customer->email,$customer->shipping_address,$amount);
        if($order_id){
            $this->addOrderedItems($order_id,$items);
            return $order_id;
        }
        return false;
    }

    private  function addOrderedItems($order_id,$items){
        foreach ($items as $item) {
            $this->BookModel->decreaseStock($item->id,$item->qty);
            $this->BookModel->increaseSales($item->id,$item->qty);
            $this->OrderedItemModel->create($order_id,$item->id,$item->qty);
        }
    }


}