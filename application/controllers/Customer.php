<?php
class Customer extends CI_Controller{

    private  $customer = null;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['url_helper']);
        $this->load->library(['session','FlashMessages','FlashData','email','CartManager','Auth']);
        $this->auth = new Auth();
        if(!Auth::isLoggedIn()){
            $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            redirect(site_url('sign-in?ref='.$link));
            exit();
        }
        $this->load->model(['CustomerModel','ActivationModel','ReadModel','CategoryModel']);
        $this->load->library(['session','FlashMessages','FlashData','email','CartManager','Auth']);
        $this->load->library('AccountActivator');
        $this->msg = new FlashMessages();

        if($this->auth->isLoggedIn()){
            if(!$this->auth->accountIsActivated()){
                redirect(site_url('unactivated'));
                exit();
            }
        }
    }

    public function index(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['customer'] = $this->CustomerModel->getCustomer(['customer_id'=>$_SESSION['customer_id']]);
        $data['msg'] = $this->msg;
        $data['title'] ='Edit Profile';
        $this->load->view('public/header',$data);
        $this->load->view('public/a_sidebar');
        $this->load->view('public/customer');
        $this->load->view('public/footer');
    }

    public function online_read(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $reads = $this->ReadModel->getCustomerBooks($_SESSION['customer_id']);
        $data['readings'] = $reads;
        $data['title'] = "Online Readings";
        $this->load->view('public/header',$data);
        $this->load->view('public/a_sidebar');
        $this->load->view('public/online_read',$data);
        $this->load->view('public/footer');
    }

    public function download_history(){
        $this->load->model(['EbookModel']);
        $ebooks = $this->EbookModel->getCustomerEbooks($_SESSION['customer_id']);

        $data['ebooks'] = $ebooks;
        $data['categories'] = $this->CategoryModel->getCategories();
        $data["title"] = "Download History";
        $this->load->view('public/header',$data);
        $this->load->view('public/a_sidebar');
        $this->load->view('public/download_history',$data);
        $this->load->view('public/footer');
    }

    public function order_history(){
        $this->load->model(['OrdersModel','OrderedItemModel']);
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Order History";
        $orders = $this->OrdersModel->getCustomerOrders($_SESSION['customer_id']);
        $data['orders'] = $orders;

        $this->load->view('public/header',$data);
        $this->load->view('public/a_sidebar');
        $this->load->view('public/order_history',$data);
        $this->load->view('public/footer');
    }

    public function order_details($order_id='-1'){
        $this->load->model(['OrdersModel','OrderedItemModel']);
        $order = $this->OrdersModel->getCustomerOrder($_SESSION['customer_id'],$order_id);
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "Order Details | TK".str_pad($order->order_id,5,'0');
        $books=$this->OrderedItemModel->getOrderedItems($order_id);

        $data['order'] =$order;
        $data['books'] = $books;
        $this->load->view('public/header',$data);
        $this->load->view('public/a_sidebar');
        $this->load->view('public/order_details',$data);
        $this->load->view('public/footer');
    }
}