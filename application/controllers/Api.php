<?php

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(['StatisticsHandler']);
        $this->stat = new StatisticsHandler();
    }

    public function chartjson(){
        $json = $this->stat->genSalesJSON();
        echo json_encode($json);
    }

    public function book_stat_json($book_id){
        $data = $this->stat->getBookSalesJSON($book_id);
        echo json_encode($data);
    }
}