<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 5/22/2018
 * Time: 3:06 PM
 */

class AdminAccountController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->library(['session','FlashMessages']);
        $this->msg = new FlashMessages();
        $this->load->model('AdminModel');
      
    }


    public  function login(){
        $admin = $this->AdminModel->getAdmin($_POST['username']);
        if($admin){
            if(password_verify($_POST['password'],$admin->password)){
                $_SESSION['username'] = $admin->username;
                redirect(site_url('admin/dashboard'));
            }else{
                $this->msg->error("Invalid Password");
            }
        }else{
            $this->msg->error("Invalid username");
        }
        redirect('admin/sign_in');
    }

    public function sign_in(){
        $this->load->view("admin/login",['msg'=>$this->msg]);
    }


    public function update(){
        $admin = $this->AdminModel->getAdmin($_SESSION['username']);
        if(password_verify($_POST['old-password'],$admin->password)){
            if($this->AdminModel->update($admin->password))
                $this->msg->success("Account details updated successfully");
            $_SESSION['username'] = $_POST['username'];
        }else{
            $this->msg->error("Invalid Old password");
        }

        redirect($_SERVER['HTTP_REFERER']);
    }
}