<?php
class Reader extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['url_helper']);
        $this->load->model(['BookModel','ReadModel','CategoryModel']);
        $this->load->library(['session','FlashMessages','FlashData','Auth','CartManager']);
        $this->auth = new Auth();
        $this->msg = new FlashMessages();
        if(!Auth::isLoggedIn()){
            $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            redirect(site_url('sign-in?ref='.$link));
            exit();
        }
        if($this->auth->isLoggedIn()){
            if(!$this->auth->accountIsActivated()){
                redirect(site_url('unactivated'));
                exit();
            }
        }
    }



    public function read($urid='-1'){
        $read = $this->ReadModel->getBook($urid,$_SESSION['customer_id']);
        $data['categories'] = $this->CategoryModel->getCategories();
        if($read){
            $book = $this->BookModel->getBook($read->book_id);
            $data['uri'] = site_url('reader/getfile/'.$urid.'.pdf');
            $data['title'] = "Reading | ".$book->title;
            $data['book_title'] = $book->title;
            $this->load->view("public/header",$data);
            $this->load->view('public/reader',$data);
            $this->load->view('public/footer');
        }else{
            $this->not_found();
        }

    }

    public function getFile($urid='none'){
        if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] == site_url('assets/js/pdfjs/pdf.worker.js')){
            $read = $this->ReadModel->getBook($urid,$_SESSION['customer_id']);
            $book = $this->BookModel->getBook($read->book_id);
            $book = 'uploads/ebooks/'.$book->ebook_file;
            if(file_exists($book)){
                header('Content-Description: File Transfer');
                header('Content-Type: application/pdf');
                header('Content-Disposition: inline; filename='.$book);
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($book));
                flush(); // Flush system output buffer
                readfile($book);
                exit;
            }else{
                $this->not_found();
            }
        }else{
            echo "<H1>Sorry You dont have permission to view this file";
        }

    }

    public function not_found(){
        $data['categories'] = $this->CategoryModel->getCategories();
        $data['title'] = "404 Not found";
        $this->load->view("public/header");
        $this->load->view('public/not_found');
        $this->load->view('public/footer');
    }



}