<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AccountController extends CI_Controller
{
    public $msg = null;
    function __construct()
    {
        parent::__construct();
        $this->load->model(['CustomerModel','ActivationModel','PasswordResetModel']);
        $this->load->library(['session','FlashMessages','FlashData','email','EmailSender','TokenManager']);
        $this->load->library('AccountActivator');

        $this->load->helper(['url_helper']);
        $this->msg = new FlashMessages();
        $this->tokenMgr = new TokenManager();
        $this->AccountActivator = new AccountActivator();



    }

    public function register(){
        $error = false;
        if($this->CustomerModel->emailExist($_POST['email'])){
            $this->msg->error("Email already in use");
            $error = true;
        }

        if ($this->CustomerModel->phoneExist($_POST['phone_no'])){
            $this->msg->error("Phone Number already in use");
            $error = true;
        }

        if ($error){
            $this->setFlashData();
            redirect(site_url('sign-up'));
        }else{
          if($this->CustomerModel->create()){
              $this->AccountActivator->sendActivationEmail($_POST['email']);
              FlashData::set('reg-success',true);
              FlashData::set('name',$_POST['first_name'].' '.$_POST['last_name']);
              FlashData::set('email',$_POST['email']);
              FlashData::set('send_email',$_POST['email']);
              redirect(site_url('sign-up'));
          }

        }

    }



    public function update(){
        $customer = $this->CustomerModel->getCustomer(['customer_id'=>$_SESSION['customer_id']]);
        $error = false;

        if($customer->email !=$_POST['email']){
            if($this->CustomerModel->emailExist($_POST['email'])){
                $this->msg->error("Email already in use");
                $error = true;
            }
        }

        if($customer->phone_no!=$_POST['phone_no']){
            if ($this->CustomerModel->phoneExist($_POST['phone_no'])){
                $this->msg->error("Phone Number already in use");
                $error = true;
            }
        }

        if(!password_verify($_POST['old-password'],$customer->password)){
            $this->msg->error('Wrong old Password');
            $error = true;
        }

        if ($error){
            $this->setFlashData();
            redirect(site_url('customer'));
        }else{
            if($this->CustomerModel->updateDetails($customer->password)){
                $this->msg->success("Account update was successfully");
                redirect(site_url('customer'));
            }
        }
    }

    public function login(){

            $cus = $this->CustomerModel->getLogin($_POST['username']);
            if($cus ==null){
                $this->msg->error("Email or Phone number does not exist");
                redirect($_SERVER['HTTP_REFERER']);

            }else{
                if(password_verify($_POST['password'],$cus->password)){
                    if(isset($_POST['remember'])){
                        $this->tokenMgr->setToken($cus->customer_id);
                    }
                    $_SESSION['customer_id'] = $cus->customer_id;
                    $_SESSION['first_name'] = $cus->first_name;

                    if(isset($_GET['ref']))
                        redirect($_GET['ref']);
                    else
                        redirect(site_url());
                }else{
                    $this->msg->error('Invalid Password');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
    }


    public  function logout(){
        unset($_SESSION['customer_id']);
        unset($_SESSION['first_name']);
        $this->tokenMgr->clearToken();
        redirect(site_url('sign-in'));
    }

    public function forget_password(){
        if(!empty($_POST['email'])){
            $customer = $this->CustomerModel->getCustomer(['email'=>$_POST['email']]);
            if($customer){
                $hash = md5(mt_rand());

                $msg = "<p>Dear <b>".$customer->first_name.' '.$customer->last_name."</b> click on the link below to reset your password</p>
          <a href='".site_url('account/reset_password/'.$hash)."'>".site_url('account/reset_password/'.$hash)."</a>";
                $this->PasswordResetModel->create($customer->customer_id,$hash);
                EmailSender::send($_POST['email'],'BookStore | Password Reset ',$msg);

                $this->msg->success("Password reset link has been sent to your email");
            }else{
                $this->msg->error("Your email could not be found");
            }

        }
        redirect($_SERVER['HTTP_REFERER']);
    }


    public function password_reset(){
        if(!empty($_POST['id'])){
            $reset = $this->PasswordResetModel->getResetData($_POST['id']);
            $pass = password_hash($_POST['password'],PASSWORD_DEFAULT);
            $this->CustomerModel->update($reset->customer_id,['password'=>$pass]);
            $this->msg->success("Password was reset was successful you can now sign in with your new password ");
            redirect(site_url('sign-in'));
            exit();
        }

        redirect(site_url('reset-password'));
    }







    public function activate(){
        $act = $this->ActivationModel->getCustomer($_GET['ref']);
        if($act != null){
                if($act->activated ==true){
                    FlashData::set('activation-ok',true);
                    FlashData::set('alreadyActivated',true);
                }else{
                    $this->CustomerModel->activate($act->customer_id);
                    FlashData::set('activation-ok',true);
                    FlashData::set('activated',true);
                }
            redirect(site_url('account/activated'));
        }else{
            $this->msg->error("Your account could not be activate due some error from the activation link");
           redirect(site_url('sign-up'));
        }

    }

    public function resend(){
        $cus = $this->CustomerModel->getCustomer(['email'=>$_GET['to']]);
        if($cus->activated ==true){
            FlashData::set('alreadyActivated',true);
            redirect(site_url('account/activated'));
        }else{
            $this->AccountActivator->sendActivationEmail($cus->email);
            $this->msg->success("Email has been resent");
            FlashData::set('reg-success',true);
            FlashData::set('name',$cus->first_name.' '.$cus->last_name);
            FlashData::set('email',$cus->email);
            FlashData::set('send_email',$cus->email);
            redirect($_SERVER['HTTP_REFERER']);
        }

    }

    private function setFlashData(){
        FlashData::set('first',$_POST['first_name']);
        FlashData::set('last',$_POST['last_name']);
        FlashData::set('male',$_POST['gender']);
        FlashData::set('female',$_POST['gender']);
        FlashData::set('phone',$_POST['phone_no']);
        FlashData::set('email',$_POST['email']);
        FlashData::set('address',$_POST['address']);
        FlashData::set('password',$_POST['password']);
        FlashData::set('password1',$_POST['password']);
    }




}