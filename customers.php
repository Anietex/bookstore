<?php
require "Core/functions/util_functions.php";
require "vendor/autoload.php";
if(!\Controllers\AuthController::isAuth())
    redirect();
getTemplate("header","Customers");
getTemplate("sidebar");

?>
<main id="customers">
    <div class="row green accent-1 alert_box" v-show="updateSuccess" style="display: none;">
        <div class="col s11">
            <p>Customer record was successfully updated</p>
        </div>
        <div class="col s1" v-on:click="updateSuccess=false">
            <i class="fa fa-close"></i>
        </div>
    </div>
    <div class="row green accent-1 alert_box" v-show="delSuccess" style="display: none;">
        <div class="col s11">
            <p>Customer record was successfully deleted</p>
        </div>
        <div class="col s1" v-on:click="delSuccess=false">
            <i class="fa fa-close"></i>
        </div>
    </div>
    <div class="row red accent-2 alert_box" v-show="ajaxFail" style="display: none;">
        <div class="col s11">
            <p>Sorry request could not be processed at moment please try again later.</p>
        </div>
        <div class="col s1" v-on:click="ajaxFail=false">
            <i class="fa fa-close"></i>
        </div>
    </div>
    <div class="row green accent-1 alert_box" v-show="codeSent" style="display: none;">
        <div class="col s11">
            <p>Code was sent successfully</p>
        </div>
        <div class="col s1" v-on:click="codeSent=false">
            <i class="fa fa-close"></i>
        </div>
    </div>
    <div class="row">
        <div class="col m4 s12">
            <h4 class="text-accent-4 teal-text">All Customers</h4>
        </div>
        <div class="col s12 m4">
            <div class="input-field">
                <select @change="sortRecord" v-model="order">
                    <option value=""   >Sort by status</option>
                    <option value="Processed">Processed</option>
                    <option value="Unprocessed">Unprocessed</option>
                </select>
            </div>
        </div>
        <div class="col m4 s12">
            <div class="input-field">

                <input id="full_name" type="text" class="" v-model="searchTerm" @keyup="filterCustomer" required>
                <label for="full_name">Search</label>
            </div>
        </div>
    </div>
    <section class="all-customers">

        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th width="">Date & Time</th>
                <th width="%">Name</th>
                <th width="20%">Topic</th>
                <th width="5%">Email</th>
                <th width="5%">Phone</th>
                <?php if($_SESSION['auth_type']=="ROOT"): ?>
                <th>Code</th>
    <?php endif;?>
                <th>Status</th>
                <th colspan="3" class="center-align">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(cus,index) in customers ">
                <td>{{cus.customer_id}}</td>
                <td>{{cus.date_added}} {{ cus.time_added }}</td>
                <td>{{ cus.full_name }}</td>
                <td>{{ cus.topic }}</td>
                <td>{{ cus.email }}</td>
                <td> {{ cus.phone }}</td>
<?php if($_SESSION['auth_type']=="ROOT"): ?>
                <td>{{ cus.code}}</td>
    <?php endif;?>
                <td>{{ cus.status }}</td>
                <td class="modal-trigger" data-target="modal1">
                    <i class="fa fa-pencil green white-text act-btn" @click="editCustomer(cus)"></i>
                </td>
                <td >
                    <i class="fa fa-trash red white-text  act-btn" @click="deleteCustomer(cus)"></i>
                </td>
<?php if($_SESSION['auth_type']=="ROOT"): ?>
                <td class="modal-trigger" data-target="sendCode" >
                    <i class="fa fa-send blue white-text  act-btn"  @click="activeCustomer=cus"></i>
                </td>
    <?php endif;?>
            </tr>

            </tbody>
        </table>

    </section>
    <div class="center-align" v-if="shownPages.length>1">
        <ul class="pagination">

            <li class=""><a href="#!" @click="prevPage"><i class="fa fa-chevron-left text-accent-4 teal-text"></i></a></li>

            <li class="waves-effect" v-bind:class="{ active: p == current }" v-for="p in shownPages" @click="pagedRecords(p)">
                <a href="#!">{{p}}</a>
            </li>

            <li class="waves-effect"><a href="#!" @click="nextPage"><i class="fa fa-chevron-right text-accent-4 teal-text"></i></a></li>
        </ul>
    </div>

<!--    edit customer modal box -->
    <div id="modal1" class="modal">
        <div class="modal-content">
            <h5>Edit Customer Data</h5>
            <form>
                <div class="input-field">
                    <input id="full_name" type="text" class="validate" v-model="customer.full_name">
                    <label for="full_name">Full Name</label>
                </div>
                <div class="input-field">
                    <input id="topic" type="text" class="validate" v-model="customer.topic">
                    <label for="topic">Topic</label>
                </div>
                <div class="input-field">
                    <input id="email" type="email" class="validate" v-model="customer.email">
                    <label for="email">Email</label>
                </div>
                <div class="input-field">
                    <input id="phone" type="text" class="validate" v-model="customer.phone">
                    <label for="phone">Phone number</label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button  class="modal-action modal-close waves-effect waves-green btn-flat red white-text">Close</button>
            <button class="modal-action modal-close waves-effect waves-green btn-flat green white-text" @click="updateCustomer">Update</button>
        </div>
    </div>

<!-- send code modal box-->
    <div id="sendCode" class="modal">
        <form @submit.prevent="sendCode">
        <div class="modal-content">
            <h5>Send Code</h5>

                <div class="input-field">
                    <input id="code" type="text" class="validate" v-model="code" required>
                    <label for="code">Enter code Here</label>
                    <p>{{ activeCustomer.topic }}</p>
                </div>

        </div>
        <div class="modal-footer">
            <button type="button"  class="modal-action modal-close waves-effect waves-green btn-flat red white-text">Close</button>
            <button type="submit" class="modal-action modal-close waves-effect waves-green btn-flat green white-text">Send</button>
        </div>
        </form>
    </div>


</main>
    <!-- Modal Structure -->

<?php
getTemplate("footer");
?>