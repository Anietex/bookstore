<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/16/2018
 * Time: 1:47 PM
 */

namespace Controllers;

use Models\CustomerModel;

class CustomerController
{       private $res = [];


    public function addCustomer(){
            $cm = new CustomerModel();
            if($cm->insertCustomer()){
                $this->res['status']="success";
            }else{
                $this->res['status']="failure";
            }
            return $this;
        }
        public function getResponse(){
            echo json_encode($this->res);
        }
        public function getCustomers(){
            $cm = new CustomerModel();
            $this->res['status'] = "success";
            $this->res['customers'] = $cm->getAllRows();
            return $this;
        }
    public function getCustomer($customer_id){
        $cm = new CustomerModel();
        $this->res['status'] = "success";
        $this->res['customers'] = $cm->getARow($customer_id);
        return $this;
    }

    public function updateCustomer(){
        $cm = new CustomerModel();
        if($cm->updateCustomer()){
            $this->res['status']="success";
        }else{
            $this->res['status']="failure";
        }
        return $this;
    }

    public function deleteCustomer(){
        $cm = new CustomerModel();
        if($cm->deleteCustomer()){
            $this->res['status']="success";
        }else{
            $this->res['status']="failure";
        }
        return $this;
    }
}