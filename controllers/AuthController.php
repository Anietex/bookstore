<?php


namespace Controllers;
use Models\UsersModel;

class AuthController
{
    public function loginUser(){
        $um = new UsersModel();
        $user = $um->getUser($_POST['username'],$_POST['password']);
        if($user){
            if($user['is_admin'] ==1){
                $_SESSION['auth_type'] = "ROOT";
                $_SESSION['user_id'] = $user['user_id'];
                $_SESSION['full_name']=$user['full_name'];
            }else{
                $_SESSION['auth_type'] = "NORMAL";
                $_SESSION['user_id'] = $user['user_id'];
                $_SESSION['full_name']=$user['full_name'];
            }
            return true;
        }
        return false;
    }
    public  static function isAuth(){
        return isset($_SESSION['user_id']);
    }

    public static  function logOut(){
        unset( $_SESSION['auth_type']);
        unset( $_SESSION['user_id']);
    }

}