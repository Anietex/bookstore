<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/18/2018
 * Time: 2:51 PM
 */

namespace Controllers;
use Models\UsersModel;

class UserController
{

    function __construct()
    {
        if($_SESSION['auth_type']!=="ROOT"){
            die(json_encode(["reply"=>"NOT ADMIN"]));
        }
    }

    private $res=[];
    public function addUser(){
        $um = new UsersModel();
        if($um->addUser()){
            $this->res["status"]="success";
        }else{
            $this->res['status']= "failure";
        }

        return $this;

    }

    public function deleteUser(){
        $um = new UsersModel();
        if($um->delete($_POST['user_id']))
            $this->res['status']="success";
        else
            $this->res['status']="failure";
        return $this;
    }

    public function updateUser(){
        $um = new UsersModel();
        if($um->updateUser())
            $this->res['status']="success";
        else
            $this->res['status']="failure";

        return $this;
    }

    public function getUsers(){
        $um = new  UsersModel();
        $this->res['users']=$um->getAllRows();
        return $this;
    }

    public function getResponse(){
       echo json_encode($this->res);
}
}