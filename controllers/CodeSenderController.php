<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/18/2018
 * Time: 10:50 AM
 */

namespace Controllers;
use Controllers\SMS\SMSController;
use Controllers\Email\EmailController;
use Models\CustomerModel;

class CodeSenderController
{
    private $cus_id;
    private $code;
    public function __construct($customer_id,$code)
    {
        $this->cus_id=$customer_id;
        $this->code=$code;
    }

    public function sendCode(){
       $this->updateCustomer();
       return $this->sendSMS() && $this->sendEmail();
    }

    public function getResponse(){

    }

    private function sendEmail(){
        $customer = $this->getCustomer();
        $mailer = new EmailController();

        $email = $customer['email'];
        $name = $customer['full_name'];
        $code = $customer['code'];
        if(empty($email)){
            return true;
        }else{
            $mailer->setSubject("ResearchClue");
            $mailer->addRecipient($email);
            $mailer->setHTMLBody(
                'Dear '.$name.' 
        Click the link below for your project material:
        <p>http://myresearchclue.com/uploads/'.$code.'.docx</p>
        <br><label>Thank you</label>
        <u><h4>TERMS OF USE</h4></u>
        <p>Only use this material as a guide in developing your original research. ResearchClue does not encourage plagiarism in anyway.</p>
        <br><label>Contact/ Email: info@researchclue.com</label><br><label>Phone: 08169050575, +2348169050575</label>');

            return $mailer->send();
        }

    }

    private function sendSMS(){
        $sms = new SMSController();
        $customer = $this->getCustomer();
        $phone = $customer['phone'];

        if(empty($phone)){
            return true;
        }else{
            $code = $customer['code'];
            $sms->addRecipient($phone);
            $sms->setSender("ProjectCode");
            $sms->setText('Your download code is '.$code.' visit www.researchclue.com to download or click http://myresearchclue.com/uploads/'.$code.'.docx. Thanks.');
            return ($sms->send()->getStatus()=="SUCCESS")?true:false;
        }
    }

    private function updateCustomer(){
        $cus  = new CustomerModel();
       return $cus->update($this->cus_id,["code"=>$this->code,"status"=>"Processed"]);
    }

    private function getCustomer(){
        $cus = new CustomerModel();
        return $cus->getARow($this->cus_id);
    }
}