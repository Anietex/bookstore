<?php


namespace Controllers\Email;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class EmailController
{
    private $mailer;

    public  function __construct()
    {
        $this->mailer = new PHPMailer(true);
        $this->mailer->isSMTP();
        $this->mailer->Host="smtpout.secureserver.net";
        $this->mailer->SMTPAuth = true;
        $this->mailer->Username ="info@researchclue.com.ng";
        $this->mailer->Password ="Active5544@";
        $this->mailer->Port="3535";
        $this->mailer->isHTML();
        $this->mailer->setFrom('info@researchclue.com.ng', 'Research Clue');

    }

    public function send(){
//
//        $mail = new PHPMailer();                              // Passing `true` enables exceptions
//        try {
//            //Server settings
//           // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
//            $mail->isSMTP();                                      // Set mailer to use SMTP
//            $mail->Host = 'smtpout.secureserver.net';  // Specify main and backup SMTP servers
//            $mail->SMTPAuth = true;                               // Enable SMTP authentication
//            $mail->Username = 'info@researchclue.com.ng';                 // SMTP username
//            $mail->Password = 'Active5544@';                           // SMTP password
//           // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
//            $mail->Port = 3535;                                    // TCP port to connect to
//
//            //Recipients
//            $mail->setFrom('info@researchclue.com.ng', 'Research Clue');
//            $mail->addAddress('anietex@gmail.com');     // Add a recipient
//
//
//            //Content
//            $mail->isHTML(true);                                  // Set email format to HTML
//            $mail->Subject = 'Probably new';
//            $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
//            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
//
//            $mail->send();
//            echo 'Message has been sent';
//        } catch (Exception $e) {
//            echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
//        }

        return $this->mailer->send();
    }

    public function addRecipient($recipient,$name=""){
        $this->mailer->addAddress($recipient,$name);
        return $this;
    }
    public function setSender($address,$name=""){
        $this->mailer->setFrom($address,$name);
        return $this;
    }

    public function setSubject($subject){
        $this->mailer->Subject = $subject;
        return $this;
    }


    public function setHTMLBody($text){
        $this->mailer->Body = $text;

    }

    public function setPlainBody($text){
        $this->mailer->AltBody =$text;
    }


}