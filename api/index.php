<?php
ob_start();
session_start();
require "../vendor/autoload.php";
require "../Core/config/pdo_connect.php";
if(!\Controllers\AuthController::isAuth()){
    echo json_encode(['status'=>'failure',"reply"=>"UNAUTHORISED ACCESS"]);
    exit();
}
 use  Controllers\CustomerController;
 use Controllers\CodeSenderController;
$cm = new CustomerController();

if(isset($_GET['request'])){
    switch ($_GET['request']){
        case "add_new":
            $cm->addCustomer()->getResponse();
            break;
        case "get_customers":
            $cm->getCustomers()->getResponse();
            break;
        case "update_customer":
            $cm->updateCustomer()->getResponse();
            break;
        case "delete_customer":
            $cm->deleteCustomer()->getResponse();
            break;
        case "send_code":
            $cs = new CodeSenderController($_POST['customer_id'],$_POST['code']);
            if($cs->sendCode())
               echo json_encode(["status"=>"success"]);
            else
                echo json_encode(["status"=>"failure"]);
            break;
        case "add_user":
            $uc = new \Controllers\UserController();
            $uc->addUser()->getResponse();
            break;
        case "update_user":
            $uc = new \Controllers\UserController();
            $uc->updateUser()->getResponse();
            break;
        case "get_users":
            $uc = new \Controllers\UserController();
            $uc->getUsers()->getResponse();
            break;
        case "delete_user":
            $uc = new \Controllers\UserController();
            $uc->deleteUser()->getResponse();
            break;
        default:
            json_encode("unknown request");
    }
}

