<?php
require "Core/functions/util_functions.php";
require "vendor/autoload.php";

if(!\Controllers\AuthController::isAuth())
    redirect();
getTemplate("header","Users");
getTemplate("sidebar");
?>
<main class="users" id="users">
    <h4 class="text-accent-4 teal-text">System Users</h4>
    <div class="row green accent-1 alert_box" v-show="addSuccess" style="display: none;">
        <div class="col s11">
            <p>User was successfully added</p>
        </div>
        <div class="col s1" v-on:click="addSuccess=false">
            <i class="fa fa-close"></i>
        </div>
    </div>
    <div class="row red accent-2 alert_box" v-show="ajaxFail" style="display: none;">
        <div class="col s11">
            <p>Sorry request could not be processed at moment please try again later.</p>
        </div>
        <div class="col s1" v-on:click="ajaxFail=false">
            <i class="fa fa-close"></i>
        </div>
    </div>

    <div class="row green accent-1 alert_box" v-show="delSuccess" style="display: none;">
        <div class="col s11">
            <p>User was successfully deleted</p>
        </div>
        <div class="col s1" v-on:click="delSuccess=false">
            <i class="fa fa-close"></i>
        </div>
    </div>

    <div class="row green accent-1 alert_box" v-show="uptSuccess" style="display: none;">
        <div class="col s11">
            <p>User record was successfully updated</p>
        </div>
        <div class="col s1" v-on:click="uptSuccess=false">
            <i class="fa fa-close"></i>
        </div>
    </div>

    <div class="row red accent-2 alert_box" v-show="!passMatch" style="display: none;">
        <div class="col s11">
            <p>Password mismatch</p>
        </div>
        <div class="col s1" v-on:click="passMatch=true">
            <i class="fa fa-close"></i>
        </div>
    </div>
    <section>
        <div class="row">
            <form class="col s12" v-on:submit.prevent="processRequest" >
                <div class="input-field col s12 m3">
                    <input id="full_name" type="text" class="validate" v-model="user.full_name" required>
                    <label for="full_name" >Full Name</label>
                    <span class="helper-text" data-error="Enter Full name"></span>
                </div>
                <div class="input-field col s12 m2">
                    <input id="topic" type="text" class="validate"  v-model="user.username"  required>
                    <label for="topic">Username</label>
                    <span class="helper-text" data-error="Enter username"></span>
                </div>
                <div class="input-field col s12 m2">
                    <input id="email" type="email" class="validate" v-model="user.email"  required>
                    <label for="email">Email</label>
                    <span class="helper-text" data-error="Enter Email"></span>
                </div>

                <div class="input-field col s12 m2">
                    <input id="password" type="text" class="validate" v-model="user.password"  required>
                    <label for="password">Password</label>
                    <span class="helper-text" data-error="Enter password"></span>
                </div>
                <div class="input-field col s12 m2">
                    <input id="confirm_password" type="text" class="validate" v-model="user.cpassword"  required>
                    <label for="confirm_password">Confirm Password</label>
                    <span class="helper-text" data-error="Confirm password"></span>
                </div>
                <div class="col s12 m1 add-btn">
                    <button type="submit" class="waves-effect waves-light btn" >{{ btnText }}</button>
                </div>
                <div class="col s12 m1 add-btn" v-if="btnText!=='Add'">
                    <button type="button" class="waves-effect waves-light btn red" @click="cancel">Cancel</button>
                </div>
            </form>
        </div>
    </section>
    <section class="records">
        <table>
            <thead>
                <th>Full Name</th>
                <th>Username</th>
                <th>Email</th>
                <th>Password</th>
                <td colspan="2" width="2%" class="center-align">Actions</td>
            </thead>
            <tbody>
                <tr v-for="user in users">
                    <td>{{ user.full_name}}</td>
                    <td>{{ user.username }}</td>
                    <td>{{ user.email }}</td>
                    <td>{{ user.password }}</td>
                    <td class="modal-trigger" data-target="modal1">
                        <i class="material-icons green white-text act-btn" @click="editUser(user)">edit</i>
                    </td>
                    <td v-if="user.is_admin!=1">
                        <i class="material-icons red white-text  act-btn" @click="deleteUser(user)">delete</i>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
</main>
<?php
getTemplate("footer");
?>
