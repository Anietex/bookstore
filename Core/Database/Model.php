<?php
namespace  Core\Database;
use PDO;
abstract class Model{

 	protected $dbh;

	protected $table;

	protected $sql;

	protected $fields;

	protected $primaryKey;

	protected $lastID;




	public function __construct(){

		global $dbh;
		$this->dbh=$dbh;

	}


	public function insert(array $data){
		$this->insertQueryBuilder($data);

				$stmt= $this->dbh->prepare($this->sql);

				if($stmt->execute($this->fields)){
				  $this->lastID=$this->dbh->lastInsertId();
					return true;
				}
				else{
					print_r($stmt->errorInfo());
				}
	}

	public  function getLastInsertId(){
	    return $this->lastID;
    }

	public function getARow($id){
		$sql ="SELECT * FROM $this->table WHERE $this->primaryKey= ?";
		$stmt=$this->dbh->prepare($sql);
		$stmt->execute([$id]);
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}
	public function execute($sql,array $vals=[]){
        $stmt=$this->dbh->prepare($sql);
        $stmt->execute($vals);
        //print_r($stmt->errorInfo());
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

	public function getAllRows(){
		$sql="SELECT * FROM $this->table ORDER BY $this->primaryKey DESC";
		$stmt=$this->dbh->prepare($sql);
		$stmt->execute();

		if($stmt->rowCount()<1)
			return false;
		else
			return $stmt->fetchAll(PDO::FETCH_ASSOC);

	}
	public function delete($id){
		$sql ="DELETE FROM $this->table WHERE $this->primaryKey = ?";
		$stmt=$this->dbh->prepare($sql);


		if($stmt->execute([$id])){
			return true;
		}else{
			print_r($stmt->errorInfo());
		}
	}

	public function update($id,array $data){
		$this->updateQueryBuilder($id,$data);
		$stmt=$this->dbh->prepare($this->sql);

		if($stmt->execute($this->fields))
				return true;
			else
				print_r($stmt->errorInfo());

	}

		private function insertQueryBuilder(array $data){
				$placeholder ="";
				$columns=array();
				$columnsNames="";

				foreach ($data as $key => $value){
					array_push($columns,$value);
					$placeholder.="? ,";
					$columnsNames.="$key,";
				}

				$placeholder=trim($placeholder,",");
				$columnsNames=trim($columnsNames,",");

				$this->sql="INSERT INTO $this->table ($columnsNames) VALUES ($placeholder)";
				$this->fields=$columns;
	}

	private function updateQueryBuilder($id,array $data){
				$placeholder ="";
				$columns=array();
				$columnsNames ="";

				foreach ($data as $key => $value){
					array_push($columns,$value);
					$placeholder.="$key = ? ,";
				}
				array_push($columns,$id);

				$placeholder=trim($placeholder,",");
				$this->sql="UPDATE $this->table SET $placeholder WHERE $this->primaryKey= ?";
				$this->fields=$columns;
	}

}

 ?>