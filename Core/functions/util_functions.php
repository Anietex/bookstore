<?php 
ob_start();
session_start();
require 'Core/config/pdo_connect.php';
function redirect($page='index.php'){
		$url="http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
		$url=rtrim($url,'/\\');
		$url.='/'.$page;
		header("Location:".$url);
	}
	function getTemplate($file,$title=''){
        $file = 'Core/template/'.$file.'.php';
        if(file_exists($file))
            require $file;
        else
            echo "Requested view file not found";
    }

    function isloggedIn(){
	    return isset($_SESSION['user_id']);
    }



 ?>