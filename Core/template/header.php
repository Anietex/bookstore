<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <script type="text/javascript" src="assets/js/vue.js"></script>
    <title><?=$title?> | Customer Transaction System</title>
</head>
<body>

<div class="load-wrapper">
    <div class="inner-wrapper">
        <img src="assets/images/radial.gif">
    </div>
</div>
<div class="ajax-load">
    <div class="ajax-wrapper">
        <img src="assets/images/ajax.gif">
    </div>
</div>

<div class="body">
<header class="navbar-fixed">
    <nav>
        <div class="nav-wrapper">
            <a href="dashboard.php" class="brand-logo">CustomersTS</a>
            <a href="#" class="sidenav-trigger"><i class="fa fa-bars"></i></a>
        </div>
    </nav>
</header>
