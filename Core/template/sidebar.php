<aside>
    <div class="side-bar">
        <ul>
            <li>
                <div class="row user">
                    <div class="col s4">
                        <img src="assets/images/avatar.png" height="50px">
                    </div>
                    <div>
                        <p><?php echo $_SESSION['full_name'] ?></p>
                    </div>
                </div>
            </li>
            <li><a href="dashboard.php"><span class="fa fa-dashboard"></span> Dashboard</a> </li>
            <li> <a href="customers.php"><span class="fa fa-edit"></span>  Customers</a> </li>
            <?php if($_SESSION["auth_type"]=="ROOT"):?>
            <li> <a href="users.php"><span class="fa fa-users"> </span> Users</a> </li>
            <?php endif;?>
            <li><a href="#" id="side-dropdown-trigger" data-target='dropdown1'><span class="fa fa-cogs" ></span> Tools</a> </li>
            <ul id='dropdown1' style="display: none">
                <li><a href="#!" target="_blank"><span class="fa fa-support"></span> Support</a></li>
                <li><a href="#!" target="_blank"><span class="fa fa-desktop"></span> System</a></li>
                <li><a href="#!" target="_blank"><span class="fa fa-paper-plane"></span>Email Checker</a></li>
            </ul>
            <li> <a href="logout.php"> <span class="fa fa-sign-out"> </span>Logout</a> </li>
        </ul>
    </div>
</aside>
