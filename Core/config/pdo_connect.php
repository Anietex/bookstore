<?php 

	define('DB_USER','root');
	define('DB_PASSWORD','');
	define('DB_HOST','localhost');
	define('DB_NAME','customer_transaction');


	try{
		$dbh = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME,DB_USER,DB_PASSWORD);

	}
	catch(PDOException $error)
	{
	    print "<pre>";
		print $error->getTraceAsString();
		print '</pre>';
	}
	