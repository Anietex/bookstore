<?php
require "Core/functions/util_functions.php";
require "vendor/autoload.php";
if(!\Controllers\AuthController::isAuth())
    redirect();

getTemplate("header","Dashboard");
getTemplate("sidebar");
?>
<main id="dashboard">
    <section class="boxes" >

        <ul class="collapsible" id="dash-collapse">
            <li class="active">
                <div class="collapsible-header "><i class="fa fa-bar-chart"></i>Summary</div>
                <div class="collapsible-body">
                    <div class="row">
                        <div class="col s12 l4 m4 ">
                            <div class="box light-blue darken-1">
                                <div class="icon center-align ">
                                    <span class="fa fa-users fa-5x"></span>
                                </div>
                                <div class="nums center-align">
                                    <h5>Customers</h5>
                                    <h4>{{ allCustomers }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 l4 m4 ">
                            <div class="box teal accent-3">
                                <div class="icon center-align ">
                                    <span class="fa fa-calendar-check-o fa-5x"></span>
                                </div>
                                <div class="nums center-align">
                                    <h5>Processed</h5>
                                    <h4>{{ processed }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 l4 m4 ">
                            <div class="box red darken-1">
                                <div class="icon center-align ">
                                    <span class="fa fa-calendar-times-o fa-5x"></span>
                                </div>
                                <div class="nums center-align">
                                    <h5>Unprocessed</h5>
                                    <h4>{{ unprocessed }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="add_cus">
                <div class="collapsible-header"><i class="fa fa-user-plus"></i>Add Customer</div>
                <div class="collapsible-body">
                    <div class="row cus-form">
                        <form class="col s12" v-on:submit.prevent="processRequest" >
                            <div class="input-field col s12 m3">
                                <i class="fa fa-user prefix"></i>
                                <input id="full_name" type="text" class="validate" v-model="customer.full_name" required>
                                <label for="full_name">Full Name</label>
                                <span class="helper-text" data-error="Enter Full name"></span>
                            </div>
                            <div class="input-field col s12 m3">
                                <i class="fa fa-text-height prefix"></i>
                                <input id="topic" type="text" class="validate"  v-model="customer.topic" required>
                                <label for="topic">Topic</label>
                                <span class="helper-text" data-error="Enter topic"></span>
                            </div>
                            <div class="input-field col s12 m2">
                                <i class="fa fa-mail-forward prefix"></i>
                                <input id="email" type="email" class="validate"  v-model="customer.email">
                                <label for="email">Email</label>
                            </div>
                            <div class="input-field col s12 m2">
                                <i class="fa fa-phone prefix"></i>
                                <input id="phone" type="text" class="validate"  v-model="customer.phone" required>
                                <label for="phone">Phone</label>
                                <span class="helper-text" data-error="Enter phone number"></span>
                            </div>
                            <div class="col s12 m1 add-btn">
                                <button type="submit" class="waves-effect waves-light btn" >{{ btnText }}</button>
                            </div>
                            <div class="col s12 m1 add-btn" v-if="btnText!=='Add'">
                                <button type="button" class="waves-effect waves-light btn red" @click="cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    <section class="add-form-d " >
        <div class="row green accent-1 alert_box" v-show="regSuccess" style="display: none;">
            <div class="col s11">
                <p>Customer was successfully added</p>
            </div>
            <div class="col s1" v-on:click="regSuccess=false">
                <i class="fa fa-close"></i>
            </div>
        </div>
        <div class="row red accent-2 alert_box" v-show="ajaxFail" style="display: none;">
            <div class="col s11">
                <p>Sorry request could not be processed at moment please try again later.</p>
            </div>
            <div class="col s1" v-on:click="ajaxFail=false">
                <i class="fa fa-close"></i>
            </div>
        </div>
        <div class="row green accent-1 alert_box" v-show="updateSuccess" style="display: none;">
            <div class="col s11">
                <p>Customer record was successfully updated</p>
            </div>
            <div class="col s1" v-on:click="updateSuccess=false">
                <i class="fa fa-close"></i>
            </div>
        </div>
        <div class="row green accent-1 alert_box" v-show="delSuccess" style="display: none;">
            <div class="col s11">
                <p>Customer record was successfully deleted</p>
            </div>
            <div class="col s1" v-on:click="delSuccess=false">
                <i class="fa fa-close"></i>
            </div>
        </div>
        <div class="row green accent-1 alert_box" v-show="codeSent" style="display: none;">
            <div class="col s11">
                <p>Code was sent successfully</p>
            </div>
            <div class="col s1" v-on:click="codeSent=false">
                <i class="fa fa-close"></i>
            </div>
        </div>
<!--        <hr>-->

    </section>
    <div class="row">
        <div class="col m4 s12">
            <h5 class="text-accent-4 teal-text">Customers</h5>
        </div>
        <div class="col s12 m4">
            <div class="input-field">
                <select @change="sortRecord" v-model="order">
                    <option value=""   >Sort by status</option>
                    <option value="Processed">Processed</option>
                    <option value="Unprocessed">Unprocessed</option>
                </select>
            </div>
        </div>
        <div class="col m4 s12">
            <div class="input-field">
                <input id="full_name" type="text" class=""  v-model="searchTerm" @keyup="filterCustomer">
                <label for="full_name">Search</label>
            </div>
        </div>
    </div>
<!--    <hr>-->
    <section class="d-customers">
        <table class="">
            <thead>
            <tr>
                <th>ID</th>
                <th width="">Date & Time</th>
                <th width="%">Name</th>
                <th width="20%">Topic</th>
                <th width="5%">Email</th>
                <th width="5%">Phone</th>
                <?php if($_SESSION['auth_type']=="ROOT"): ?>
                <th>Code</th>
                <?php endif; ?>
                <th>Status</th>
                <th colspan="3" class="center-align">Action</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(cus,index) in customers ">
                <td>{{cus.customer_id}}</td>
                <td>{{cus.date_added}} {{ cus.time_added }}</td>
                <td>{{ cus.full_name }}</td>
                <td>{{ cus.topic }}</td>
                <td>{{ cus.email }}</td>
                <td> {{ cus.phone }}</td>

                <?php if($_SESSION['auth_type']=="ROOT"): ?>
                <td>{{ cus.code}}</td>
                <?php endif; ?>

                <td>{{ cus.status }}</td>
                <td>
                    <i class="fa fa-pencil green white-text act-btn" @click="editCustomer(cus)"></i>
                </td>
                <td>
                    <i class="fa fa-trash red white-text  act-btn" @click="deleteCustomer(cus)"></i>
                </td>
                <?php if($_SESSION['auth_type']=="ROOT"): ?>
                <td class="modal-trigger" data-target="sendCode" >
                    <i class="fa fa-send blue white-text  act-btn"  @click="activeCustomer=cus"></i>
                </td>
                <?php endif; ?>
            </tr>
            </tbody>
        </table>
    </section>

    <!-- send code modal box-->
    <div id="sendCode" class="modal">
        <form @submit.prevent="sendCode">
        <div class="modal-content">
            <h5>Send Code</h5>
                <div class="input-field">
                    <input id="code" type="text" class="validate" v-model="code" required>
                    <label for="code">Enter code Here</label>
                </div>
                <p>{{ activeCustomer.topic }}</p>
        </div>
        <div class="modal-footer">
            <button  type="button" class="modal-action modal-close waves-effect waves-green btn-flat red white-text">Close</button>
            <button type="submit" class="modal-action modal-close waves-effect waves-green btn-flat green white-text" >Send</button>
        </div>
        </form>
    </div>
</main>
<?php
getTemplate("footer");
?>

