



(function () {
   $("#delete-modal").on('show.bs.modal',function (event) {
       var btn = $(event.relatedTarget);
       var form = $("#delete-form");

       var action = form.attr('action')+btn.data('id');
       form.attr('action',action);
   })
})();













$("#cover-upload").on('change',function (event) {
    $('#cover_photo').attr('src',URL.createObjectURL(event.target.files[0]))
});
$(document).ready(function () {
    $("#books-table").DataTable();
    $("#orders-table").DataTable();
})

$("#dropdown-btn").click(function (e) {
    e.preventDefault();
    $("#dropdown").slideToggle();
    $("#d-icon").toggleClass('fa-chevron-left').toggleClass('fa-chevron-down');
})


$.get('/bookstore/api/chartjson',function (data) {
  loadChart(JSON.parse(data));
});

(function (elm) {
    var url = elm.data('url');
    $.get(url,function (data) {
        var data = JSON.parse(data);
        loadBuyingStat(data.buying_stat);
        loadBookChart(data.sales_json);
    })
})($('#buying-pie'));

$("aside a").each(function (index,el) {
    var uri = document.documentURI;
    var f = uri.substr(uri.lastIndexOf('/')+1);

    if($(el).attr("href")===uri){
        $(el).parent().addClass('active');
    }
});

(function () {
    $("#sidebar-toggle").click(function (event) {
        $("#side-bar").slideToggle(200);
    });
})();



$("main").click(function () {
  //  $("#side-bar").slideUp(200);
});



$("#total-orders").circliful({
    animationStep: 10,
    foregroundBorderWidth: 5,
    foregroundColor:'#4CAF50',
    backgroundBorderWidth: 10,
    percentageTextSize:20,
    textSize:10,
    textBelow:true,
    percent: 100,
    text:'Completed Orders'
});



$("#confirm-orders").circliful({
    animationStep: 10,
    foregroundBorderWidth: 5,
    foregroundColor:'#FFC107',
    backgroundBorderWidth: 10,
    percentageTextSize:20,
    textSize:10,
    textBelow:true,
    percent: $(this).data('value'),
    text:'Confirm Orders '
});

$("#unconfirm-orders").circliful({
    animationStep: 10,
    foregroundBorderWidth: 5,
    foregroundColor:'#F44336',
    backgroundBorderWidth: 10,
    percentageTextSize:20,
    textSize:10,
    textBelow:true,
    percent: $(this).data('value'),
    text:'Unconfirmed Orders'
});

function loadBuyingStat(data) {
    console.log(data)
    Highcharts.chart('buying-pie', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        credits:false,
        title: {
            text: 'Book Buying Statistics'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Sales',
            colorByPoint: true,
            data: [{
                name: 'Hard Copy',
                y: Number.parseInt(data.hardcopy)
            }, {
                name: 'Ebook Download',
                y: Number.parseInt(data.ebook)
            }, {
                name: 'Read Online',
                y:Number.parseInt(data.readings)
            }]
        }]
    });
}

function loadBookChart(data) {
     Highcharts.chart('sales-stat', {
        chart: {
            type: 'area'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Sales Revenue'
        },
        subtitle:{
            text:'Last 10 days Revenue statistics'
        },
        xAxis: {
            categories: data.days.reverse(),
            title: {
                text: 'Days'
            }
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            labels: {
                formatter: function () {
                    return this.value / 1000 + 'k';
                }
            }
        },
        series: [{
            name: 'Hard Copy Orders',
            data: data.orders.reverse(),
            color:'#03A9F4'
        }, {
            name: 'Online Readings Sales',
            data:data.readings.reverse(),
            color:'#F44336'
        },
            {
                name:"Ebook Sales",
                data:data.ebook.reverse(),
                color:'#FFEB3B'
            }
        ]
    });

}

function loadChart(data) {
    var myChart = Highcharts.chart('container', {
        chart: {
            type: 'area'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Sales Revenue'
        },
        subtitle:{
            text:'Last 30 days Revenue statistics'
        },
        xAxis: {
            categories: data.days.reverse(),
            title: {
                text: 'Days'
            }
        },
        yAxis: {
            title: {
                text: 'Revenue'
            },
            labels: {
                formatter: function () {
                    return this.value / 1000 + 'k';
                }
            }
        },
        series: [{
            name: 'Hard Copy Orders',
            data: data.orders.reverse(),
            color:'#03A9F4'
        }, {
            name: 'Online Readings Sales',
            data:data.readings.reverse(),
            color:'#F44336'
        },
            {
                name:"Ebook Sales",
                data:data.ebook.reverse(),
                color:'#FFEB3B'
            }
        ]
    });
}


