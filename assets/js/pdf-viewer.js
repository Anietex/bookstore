window.onload=function () {
    var pdfDoc,
        currentPage,
        totalPages,
        pageRendering =0,
        canvas = $("#viewer").get(0),
        context = canvas.getContext('2d');
    var renderingScale = null;



    window.addEventListener('resize',drawCanvas);
    drawCanvas();
    $("#next-btn").click(nextPage);
    $("#prev-btn").click(prevPage);
    $("#btn-zoom-in").click(zoomIn);
    $("#btn-zoom-out").click(zoomOut);

    function showPDF(pdfFile) {
        PDFJS.getDocument({url:pdfFile}).then(function (doc) {
            pdfDoc = doc;
            totalPages = pdfDoc.numPages;
            $("#total").text(totalPages);
            $("#loader").hide();
            showPage(1)
        }).catch(function (error) {
            $("#loader").hide();
            alert(error.message)
        })


    }

    function showPage(pageNum) {
        pageRendering = 1;
        $("#loader").show();
        $(".pages").hide();

        currentPage = pageNum;
        pdfDoc.getPage(pageNum).then(function (page) {
            var scale = canvas.width/page.getViewport(1).width;
            if(renderingScale ===null)
                renderingScale = scale;


          // resizeCanvas();
            var vp = page.getViewport(renderingScale);
            canvas.height = vp.height;
            
            var renderContext={
                canvasContext:context,
                viewport:vp
            };

            page.render(renderContext).then(function () {
                pageRendering = 0;
            });
            $("#current").text(pageNum)
            $("#loader").hide();
            $(".pages").show();
        })

    }
    
    
    function nextPage() {

        if(currentPage!==totalPages){
            showPage(++currentPage);
        }
    }

    
    function prevPage() {
        if(currentPage!==1){
            showPage(--currentPage);
        }
    }

    function zoomIn() {
        renderingScale-=.2;
        resizeCanvas()
        showPage(currentPage);
    }
    function zoomOut() {
        renderingScale+=.2;
        resizeCanvas();
        showPage(currentPage);
    }

    function resizeCanvas(){
        var width = document.documentElement.clientWidth;
        renderingScale = null;

        if(width >= 1200)
            canvas.width = 1000;
        else if(width>992)
            canvas.width = 900;
        else if(width>768)
            canvas.width = 700;
        else if(width>=550)
            canvas.width = 500;
        else
            canvas.width = 300;
    }


    function drawCanvas() {
        $("#loader").show();
        resizeCanvas();
        showPDF(canvas.dataset.uri);
    }







};
