$('#pay-btn').click(function (e) {
    payWithPaystack(e)
});
$('#check-out-pay').click(function (e) {
    var target = $(e.target);
    disableBtn(target);
    var handler = PaystackPop.setup({
        key: 'pk_test_7962f86b337f5a3b913a1f777581d03d0e04dfb0',
        email: target.data('email'),
        amount: target.data('amount'),
        metadata: {
            custom_fields:{
                customer:{
                    customer_id:target.data('customer_id'),
                    shipping_address:target.data('shipping_address'),
                    email:target.data('email'),
                    phone_no:target.data('phone_no')
                },
               cart:data.map(function (item) {
                   delete(item.title);
                   delete (item.copies);
                   delete (item.cover);
                   delete (item.subtotal);
                   delete (item.title);
                   delete (item.price);
                   return item;
               })
            }
        },
        callback: function(response){
           window.location = target.data('callback')+"/"+response.reference
        },
        onClose: function(){
          enableBtn(target);
        }
    });
    handler.openIframe();

});

function checkoutPayment(e){
    var target = $($(e.target));
    disableBtn(target);
    var handler = PaystackPop.setup({
        key: 'pk_test_7962f86b337f5a3b913a1f777581d03d0e04dfb0',
        email: target.data('email'),
        amount: target.data('amount'),
        metadata: {
            custom_fields:{
                customer:{
                    customer_id:target.data('customer_id')
                },
                book:{
                    book_id:target.data('book_id')
                }
            }
        },
        callback: function(response){
            window.location = target.data('callback')+"/"+response.reference
            //   alert('success. transaction ref is ' + response.reference);
        },
        onClose: function(){
            enableBtn(target);
        }
    });
    handler.openIframe();
}










function payWithPaystack(e){
    var target = $($(e.target));
   disableBtn(target);
    var handler = PaystackPop.setup({
        key: 'pk_test_7962f86b337f5a3b913a1f777581d03d0e04dfb0',
        email: target.data('email'),
        amount: target.data('amount'),
        metadata: {
            custom_fields:{
                customer:{
                    customer_id:target.data('customer_id')
                },
                book:{
                    book_id:target.data('book_id')
                }
            }
        },
        callback: function(response){
            window.location = target.data('callback')+"/"+response.reference
        },
        onClose: function(){
            enableBtn(target)
        }
    });
    handler.openIframe();
}

function disableBtn(btn) {
    btn.attr('disabled',true);
    btn.css('cursor','not-allowed')
}

function enableBtn(btn) {
    btn.attr('disabled',false);
    btn.css('cursor','pointer')
}