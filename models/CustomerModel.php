<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/16/2018
 * Time: 1:48 PM
 */


namespace Models;
use Core\Database\Model;

class CustomerModel extends  Model

{
    protected $table ="customers";
    protected $primaryKey ="customer_id";

    public function insertCustomer(){

        //print_r($_POST);
        date_default_timezone_set("Africa/Lagos");
        $data = ["date_added"=>date('Y-m-d'),
            "time_added"=>date('g:i a'),
            "full_name"=>$_POST['full_name'],
            "topic"=>$_POST['topic'],
            "email"=>$_POST['email'],
            "phone"=>$_POST["phone"],
            "code"=>"",
            "status"=>"Unprocessed"
            ];
       return $this->insert($data);
       // return true;
    }



    public function updateCustomer(){
        $data = [
            "full_name"=>$_POST['full_name'],
            "topic"=>$_POST['topic'],
            "email"=>$_POST['email'],
            "phone"=>$_POST["phone"]
        ];
        return $this->update($_POST['customer_id'],$data);

    }

    public function deleteCustomer(){
        return $this->delete($_POST['customer_id']);
    }
}