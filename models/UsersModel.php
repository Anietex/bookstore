<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/18/2018
 * Time: 2:52 PM
 */

namespace Models;


use Core\Database\Model;

class UsersModel extends  Model
{
    protected $table = "users";
    protected $primaryKey="user_id";

    public function addUser(){
        $data = ['full_name'=>$_POST['full_name'],
            "username"=>$_POST['username'],
            "email"=>$_POST['email'],
            "password"=>$_POST['password']];

        return $this->insert($data);


    }

    public function updateUser(){
        $data = ['full_name'=>$_POST['full_name'],
            "username"=>$_POST['username'],
            "email"=>$_POST['email'],
            "password"=>$_POST['password']];
        return $this->update($_POST['user_id'],$data);
    }

    public function getUser($user,$pass){
       $users = $this->execute("SELECT * FROM users WHERE username =? and password =?",[$user,$pass]);
       if(count($users)>0){
           return $users[0];
       }else{
           return false;
       }
    }



}